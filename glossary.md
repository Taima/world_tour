## Glossary

-   CHEESING: To perform an action that intentionally evades the restrictions, violating the honorable spirit of the World Tour Challenge.
-   ITEM(S): Any Equip, Use, Set-up, Etc, Cash item, Gachapon ticket, monster card, or NX.
-   IRONMAN: A character with additional restrictions present from character creation that forbid parties of more than one character, trading or transferring of items or Mesos to the character (giving items away to non-ironmen is permitted), pickup of items or Mesos that do not belong to the character, and leech of any kind.
-   MAPLE WORLD: Any area that cannot be accessed during the World Tour Challenge. These areas can only be accessed before a Tourist’s first entry into the World Tour, or if a given exception allows travel outside the World Tour. These areas can be reached by leaving the World Tour through dialogue with Spinel, or via teleport rock. For the purpose of this World Tour Challenge, the following areas are considered to be part of Maple World and are not accessible without an exception:
    -   Singapore (CBD, Ulu City, Boat Quay Town, Ghost Ship)
    -   Malaysia (Trend Zone Metropolis, Kampung Village, Fantasy Theme Park)
    -   Taiwan (Ximending, Night Market, Taipei 101)
    -   Masteria (New Leaf City, Krakian Jungle, Haunted House, Phantom Forest, Crimsonwood Keep)
-   RONIN: A Tourist with additional restrictions that take effect the moment they first enter the World Tour: these characters may never leave the World Tour for any purpose, with no exceptions. They may not accept any items and Mesos that were acquired outside the World Tour, may not job advance again and thus are permanently locked to Beginner or 1st job. They may still form parties with any other character.
-   TOURIST: Any character participating in the World Tour Challenge
-   WHITED/WHITING: Dealing the most damage to a monster compared to other characters, causing the character to receive EXP in a white-colored text
-   WORLD TOUR: All areas that can be accessed without exception during the World Tour Challenge. Specifically, this only includes Spinel’s World Tour destinations, including all areas, maps, and instances connected to those destinations via map warp spots or NPC dialogue (excluding talking to Spinel to leave the World Tour). In MapleLegends, Spinel allows travel to the following areas:
    -   Shanghai of China
    -   Floating Market of Thailand
    -   Mushroom Shrine of Japan
    -   Shaolin Temple of China
    -   Golden Temple of Thailand
-   WORLD TOUR CHALLENGE: An area locked MapleStory challenge, based on the MapleLegends v0.62 private server’s implementation of the World Tour. This area locked challenge includes only World Tour areas accessible via Spinel. Characters undergoing this challenge are bound by rules to restrict gameplay largely to Spinel’s World Tour maps.
