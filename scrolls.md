## Scrolls

### For Weapons

#### For 1H Swords/One-Handed Swords

|    name | effect        | dropped by                                                                                                            | awarded by                               |
|--------:|:--------------|:----------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 60% | Watk+2, STR+1 | Zombie Mushroom, Giant Centipede, Blue Mushmom, Ravana, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For 2H Swords/Two-Handed Swords

|    name | effect                | dropped by                                                                                                                                                                                     | awarded by                               |
|--------:|:----------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, STR+3, Wdef+1 | Blue Mushroom                                                                                                                                                                                  |                                          |
| ATT 30% | Watk+5, STR+3, Wdef+1 | Cloud Fox, Black Bear Swordsman                                                                                                                                                                |                                          |
| ATT 60% | Watk+2, STR+1         | Dark Stone Golem, Bain, White Tiger Swordsman, Eagle Swordsman, Female Thief, Male Thief, Giant Centipede, Blue Mushmom, Ravana, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For 1H Axes/One-Handed Axes

|    name | effect                | dropped by                                                                                   | awarded by                               |
|--------:|:----------------------|:---------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, STR+3, Wdef+1 | Green Mushroom                                                                               |                                          |
| ATT 30% | Watk+5, STR+3, Wdef+1 | Cloud Fox, Red Goblin                                                                        |                                          |
| ATT 60% | Watk+2, STR+1         | Giant Centipede, Blue Mushmom, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For 2H Axes/Two-Handed Axes

|    name | effect                | dropped by                                                                                                                    | awarded by                               |
|--------:|:----------------------|:------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 30% | Watk+5, STR+3, Wdef+1 | Bain                                                                                                                          |                                          |
| ATT 60% | Watk+2, STR+1         | Paper Lantern Ghost, Dark Stone Golem, Giant Centipede, Ravana, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For 1H Maces/1H Blunt Weapons/1H BWs/One-Handed BWs

|    name | effect        | dropped by                                                                     | awarded by                               |
|--------:|:--------------|:-------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 60% | Watk+2, STR+1 | Giant Centipede, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For 2H Maces/2H Blunt Weapons/2H BWs/Two-Handed BWs

|    name | effect                | dropped by                                                                                                                                                          | awarded by                               |
|--------:|:----------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, STR+3, Wdef+1 | Duck, Cow                                                                                                                                                           |                                          |
| ATT 30% | Watk+5, STR+3, Wdef+1 | Red Slime                                                                                                                                                           |                                          |
| ATT 60% | Watk+2, STR+1         | Dark Stone Golem, Bronze Staffman, Mini Bronze Martial Artist, Giant Centipede, Blue Mushmom, Ravana, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For Daggers

|    name | effect                | dropped by                                                                                                                                                                               | awarded by                               |
|--------:|:----------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 30% | Watk+5, LUK+3, Wdef+1 | Big Cloud Fox, Nightghost                                                                                                                                                                |                                          |
| ATT 60% | Watk+2, LUK+1         | Black Sheep, Censer, Silver Giant, Silver Spearman, Golden Giant, Mini Gold Martial Artist, Giant Centipede, Blue Mushmom, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |
| ATT 70% | Watk+2, LUK+1         | Extra D                                                                                                                                                                                  |                                          |

#### For Spears

|    name | effect                | dropped by                                                                                           | awarded by                               |
|--------:|:----------------------|:-----------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, STR+3, Wdef+1 | Zombie Mushroom                                                                                      |                                          |
| ATT 60% | Watk+2, STR+1         | Giant Centipede, Blue Mushmom, Ravana, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For Polearms/Pole Arms

|    name | effect                | dropped by                                                                     | awarded by                               |
|--------:|:----------------------|:-------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, STR+3, Wdef+1 | Paper Lantern Ghost                                                            |                                          |
| ATT 30% | Watk+5, STR+3, Wdef+1 | Dark Stone Golem                                                               |                                          |
| ATT 60% | Watk+2, STR+1         | Giant Centipede, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For Wands

|           name | effect                | dropped by                                                                                           | awarded by                               |
|---------------:|:----------------------|:-----------------------------------------------------------------------------------------------------|:-----------------------------------------|
| Magic Att. 10% | Matk+5, INT+3, Mdef+1 | Cow, Plow Ox                                                                                         |                                          |
| Magic Att. 30% | Matk+5, INT+3, Mdef+1 | Female Boss                                                                                          |                                          |
| Magic Att. 60% | Matk+2, INT+1         | Samiho, Giant Centipede, Blue Mushmom, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |
| Magic Att. 70% | Matk+2, INT+1         | Dreamy Ghost                                                                                         |                                          |

#### For Staves/Staffs

|           name | effect                | dropped by                                                                                                                                           | awarded by                               |
|---------------:|:----------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| Magic Att. 30% | Matk+5, INT+3, Mdef+1 | Black Crow                                                                                                                                           |                                          |
| Magic Att. 60% | Matk+2, INT+1         | Samiho, Golden Giant, Mini Gold Martial Artist, Maverick V, Giant Centipede, Jiaoceng, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For Bows

|    name | effect        | dropped by                                                                                                            | awarded by                               |
|--------:|:--------------|:----------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 60% | Watk+2, Acc+1 | Sheep, Goat, Maverick V, Giant Centipede, Blue Mushmom, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |
| ATT 70% | Watk+2, Acc+1 | Water Goblin                                                                                                          |                                          |

#### For Crossbows

|    name | effect               | dropped by                                                                             | awarded by                               |
|--------:|:---------------------|:---------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, Acc+3, DEX+1 | Silver Giant, Silver Spearman, Golden Giant, Mini Gold Martial Artist, Blue Mushmom    |                                          |
| ATT 30% | Watk+5, Acc+3, DEX+1 | Dreamy Ghost                                                                           |                                          |
| ATT 60% | Watk+2, Acc+1        | Lucida, Giant Centipede, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For Claws

|    name | effect               | dropped by                                                                                                                             | awarded by                               |
|--------:|:---------------------|:---------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, Acc+3, LUK+1 | Wooden Fish                                                                                                                            |                                          |
| ATT 30% | Watk+5, Acc+3, LUK+1 | Nightghost                                                                                                                             |                                          |
| ATT 60% | Watk+2, Acc+1        | Green Mushroom, Female Thief, Male Thief, Giant Centipede, Blue Mushmom, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For Knucklers

|    name | effect                | dropped by                                                           | awarded by                               |
|--------:|:----------------------|:---------------------------------------------------------------------|:-----------------------------------------|
| ATT 10% | Watk+5, STR+3, Wdef+1 | Dark Stone Golem, Ligator                                            |                                          |
| ATT 60% | Watk+2, STR+1         | Croco, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

#### For Guns

|    name | effect        | dropped by                                                            | awarded by                               |
|--------:|:--------------|:----------------------------------------------------------------------|:-----------------------------------------|
| ATT 60% | Watk+2, Acc+1 | Lucida, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward |

### For Armor

#### For Hats/Helmets

|         name | effect                | dropped by                               | awarded by                                |
|-------------:|:----------------------|:-----------------------------------------|:------------------------------------------|
| Accuracy 60% | DEX+1, Acc+2          | Bain                                     |                                           |
|      DEF 10% | Wdef+5, Mdef+3, Acc+1 | Jiaoceng                                 |                                           |
|      DEF 30% | Wdef+5, Mdef+3, Acc+1 | Crow                                     |                                           |
|      DEF 60% | Wdef+2, Mdef+2        | Orange Mushroom, Censer, Giant Centipede | “The Secret Operation” random reward      |
|       HP 10% | MaxHP+30              | Jiaoceng                                 |                                           |
|       HP 30% | MaxHP+30              |                                          | “Sakura, the Kitty and the Orange Marble” |
|       HP 60% | MaxHP+10              | Wooden Fish                              | “The Secret Operation” random reward      |
|      INT 30% | INT+3                 | Nightghost                               |                                           |

#### For Earrings

|    name | effect                | dropped by                                                                | awarded by                                                                     |
|--------:|:----------------------|:--------------------------------------------------------------------------|:-------------------------------------------------------------------------------|
| DEX 70% | DEX+2                 | Dreamy Ghost                                                              |                                                                                |
| INT 10% | Matk+5, INT+3, Mdef+1 | Duck, Sheep, Goat, Black Goat, Wooden Fish, Jiaoceng                      |                                                                                |
| INT 60% | Matk+2, INT+1         | Maverick B, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster | “Eliminating Blue Mushmom” random reward; “The Secret Operation” random reward |
| LUK 60% | LUK+2                 | Maverick B, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster |                                                                                |

#### For Eye Accessory/Eye Accessories

|          name | effect        | dropped by                           | awarded by                                |
|--------------:|:--------------|:-------------------------------------|:------------------------------------------|
|  Accuracy 10% | Acc+3, DEX+1  | Ashigaru, Chunin, Castellan Toad     | “Collecting Info” random reward           |
|  Accuracy 30% | Acc+3, DEX+1  | Jonin, Kacchuu Musha, Castellan Toad | “Defeat the Great Offender” random reward |
|  Accuracy 60% | Acc+2         | Genin, Overlord, Castellan Toad      | “Collecting Info” random reward           |
|  Accuracy 70% | Acc+2         | Kunoichi, Castellan Toad             | “Defeat the Great Offender” random reward |
| Accuracy 100% | Acc+1         | Castellan Toad                       | “Collecting Info” random reward           |
|       INT 10% | INT+3, Mdef+2 | Castellan Toad                       | “A Peaceful World” random reward          |
|       INT 30% | INT+3, Mdef+2 | Ninto, Kacchuu Musha, Castellan Toad | “Defeat the Great Offender” random reward |
|       INT 60% | INT+1, Mdef+1 | Jonin, Castellan Toad                | “A Peaceful World” random reward          |
|       INT 70% | INT+1, Mdef+1 | Castellan Toad                       | “Defeat the Great Offender” random reward |
|      INT 100% | INT+1         | Castellan Toad                       | “A Peaceful World” random reward          |

#### For Face Accessory/Face Accessories

|              name | effect         | dropped by                                                                               | awarded by                                |
|------------------:|:---------------|:-----------------------------------------------------------------------------------------|:------------------------------------------|
|  Avoidability 10% | Avoid+2, DEX+2 | Ashigaru, Ninto, Castellan Toad                                                          | “Cooperation” random reward               |
|  Avoidability 30% | Avoid+2, DEX+2 | Male Thief, Kacchuu Musha, Castellan Toad                                                | “Defeat the Great Offender” random reward |
|  Avoidability 60% | Avoid+1, DEX+1 | Afterlord, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster, Castellan Toad | “Cooperation” random reward               |
|  Avoidability 70% | Avoid+1, DEX+1 | Kunoichi, Castellan Toad                                                                 | “Defeat the Great Offender” random reward |
| Avoidability 100% | Avoid+1        | Castellan Toad                                                                           | “Cooperation” random reward               |
|            HP 10% | MaxHP+30       | Castellan Toad                                                                           | “Helping Others” random reward            |
|            HP 30% | MaxHP+30       | Genin, Kacchuu Musha, Castellan Toad                                                     | “A Little Mischief” random reward         |
|            HP 60% | MaxHP+15       | Chunin, Castellan Toad                                                                   | “Helping Others” random reward            |
|            HP 70% | MaxHP+15       | Castellan Toad                                                                           | “A Little Mischief” random reward         |
|           HP 100% | MaxHP+5        | Castellan Toad                                                                           | “Helping Others” random reward            |

#### For Overalls/Overall Armor

|    name | effect                   | dropped by                                         | awarded by                           |
|--------:|:-------------------------|:---------------------------------------------------|:-------------------------------------|
| DEF 10% | Wdef+5, Mdef+3, MaxHP+10 | Jiaoceng                                           |                                      |
| DEF 30% | Wdef+5, Mdef+3, MaxHP+10 | Extra A                                            |                                      |
| DEF 60% | Wdef+2, Mdef+1           |                                                    | “The Secret Operation” random reward |
| DEF 70% | Wdef+2, Mdef+1           | Big Cloud Fox, Female Boss                         |                                      |
| DEX 10% | DEX+5, Acc+3, Speed+1    | Jiaoceng                                           |                                      |
| DEX 30% | DEX+5, Acc+3, Speed+1    | Extra B                                            | “Read the Newspaper!”                |
| DEX 60% | DEX+2, Acc+1             | Wooden Fish, Giant Centipede, Ravana               | “The Secret Operation” random reward |
| DEX 70% | DEX+2, Acc+1             | Coolie Zombie                                      |                                      |
| INT 30% | INT+5, Mdef+3, MaxMP+10  | Dunas (level 174)                                  |                                      |
| INT 60% | INT+2, Mdef+1            | Jiaoceng                                           |                                      |
| INT 70% | INT+2, Mdef+1            | Nightghost                                         |                                      |
| LUK 30% | LUK+5, Avoid+3, Acc+1    | The Boss                                           |                                      |
| LUK 60% | LUK+2, Avoid+1           | Water Goblin, Red Lizard, Prototype Lord, Jiaoceng |                                      |
| LUK 70% | LUK+2, Avoid+1           | Water Goblin                                       |                                      |
| STR 30% | STR+5, Wdef+3, MaxHP+5   | Jiaoceng                                           |                                      |

#### For Topwear/Tops

|    name | effect                    | dropped by                     | awarded by                                                                  |
|--------:|:--------------------------|:-------------------------------|:----------------------------------------------------------------------------|
| DEF 10% | Wdef+5, Mdef+3, MaxHP+10  | Jiaoceng                       |                                                                             |
| DEF 30% | Wdef+5, Mdef +3, MaxHP+10 | Paper Lantern Ghost, Red Slime |                                                                             |
| DEF 60% | Wdef+2, Mdef+1            | Giant Centipede, Ravana        | “Gathering information” random reward; “The Secret Operation” random reward |
|  HP 30% | MaxHP+30                  | The Boss                       |                                                                             |
| LUK 10% | LUK+3, Avoid+3            | Female Thief                   |                                                                             |
| LUK 30% | LUK+3, Avoid+3            | Bergamot                       |                                                                             |
| STR 30% | STR+3                     | Nibelung                       |                                                                             |

#### For Bottomwear/Bottoms

|     name | effect                   | dropped by                                                                         | awarded by                                                           |
|---------:|:-------------------------|:-----------------------------------------------------------------------------------|:---------------------------------------------------------------------|
|  DEF 10% | Wdef+5, Mdef+3, MaxHP+10 | Blue Mushroom, Water Goblin, Duck, Sheep, Female Thief, Male Thief, Jiaoceng       |                                                                      |
|  DEF 60% | Wdef+2, Mdef+1           | Ravana                                                                             | “Sealing Ravana” random reward; “The Secret Operation” random reward |
|  DEX 30% | DEX+3, Acc+2, Speed+1    | The Boss                                                                           |                                                                      |
|  DEX 60% | DEX+2, Acc+1             | Afterlord, Overlord, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster |                                                                      |
|   HP 60% | MaxHP+15                 | Maverick Y                                                                         |                                                                      |
|   HP 70% | MaxHP+15                 | Male Boss                                                                          |                                                                      |
| Jump 30% | Jump+4, Avoid+2          | Male Boss                                                                          |                                                                      |

#### For Capes

|            name | effect                   | dropped by                                                             | awarded by                               |
|----------------:|:-------------------------|:-----------------------------------------------------------------------|:-----------------------------------------|
|         DEX 10% | DEX+3                    | Green Mushroom                                                         |                                          |
|         DEX 30% | DEX+3                    | Nightghost, Black Crow                                                 |                                          |
|         DEX 60% | DEX+2                    | Maverick A                                                             | “Eliminating Blue Mushmom” random reward |
|          HP 30% | MaxHP+20                 | Black Crow                                                             |                                          |
|          HP 70% | MaxHP+10                 | Male Boss, Black Crow, The Boss                                        |                                          |
|         INT 10% | INT+3                    | Jr. Cerebes, Cow, Plow Ox                                              |                                          |
|         INT 30% | INT+3                    | Nightghost, Leader B                                                   |                                          |
|         INT 60% | INT+2                    |                                                                        | “Eliminating Blue Mushmom” random reward |
|         INT 70% | INT+2                    | Nightghost                                                             |                                          |
|         LUK 10% | LUK+3                    | Golden Giant, Mini Gold Martial Artist, Jiaoceng                       |                                          |
|         LUK 30% | LUK+3                    | Male Boss                                                              |                                          |
|         LUK 60% | LUK+2                    | Bain, White Tiger Swordsman, Eagle Swordsman, Female Thief, Male Thief | “Eliminating Blue Mushmom” random reward |
|         LUK 70% | LUK+2                    | The Boss                                                               |                                          |
|  Magic Def. 10% | Mdef+5, Wdef+3, MaxMP+10 | Ligator                                                                |                                          |
|  Magic Def. 60% | Mdef+3, Wdef+1           | Censer                                                                 | “The Secret Operation” random reward     |
|          MP 30% | MaxMP+20                 | Extra B                                                                |                                          |
|          MP 60% | MaxMP+10                 | Firebomb                                                               |                                          |
|          MP 70% | MaxMP+10                 | Extra D, Black Crow                                                    |                                          |
|         MP 100% | MaxMP+5                  | Orange Mushroom                                                        |                                          |
|         STR 30% | STR+3                    | Leader A                                                               |                                          |
|         STR 60% | STR+2                    | Censer, Bronze Staffman, Mini Bronze Martial Artist, Prototype Lord    | “Eliminating Blue Mushmom” random reward |
|         STR 70% | STR+2                    | Dreamy Ghost                                                           |                                          |
|        STR 100% | STR+1                    | Extra D                                                                |                                          |
| Weapon Def. 10% | Wdef+5; Mdef+3; MaxHP+10 | Big Cloud Fox                                                          |                                          |
| Weapon Def. 60% | Wdef+3, Mdef+1           | Bronze Staffman, Mini Bronze Martial Artist                            | “The Secret Operation” random reward     |

#### For Gloves

|           name | effect                | dropped by                                                                                                                                  | awarded by                                                                     |
|---------------:|:----------------------|:--------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------|
|        ATT 10% | Watk+3                | Jonin, Silver Giant, Silver Spearman, Golden Giant, Mini Gold Martial Artist, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster |                                                                                |
|        ATT 60% | Watk+2                |                                                                                                                                             | “Eliminating Blue Mushmom” random reward; “The Secret Operation” random reward |
|        DEX 10% | Acc+5, DEX+3, Avoid+1 | Jiaoceng                                                                                                                                    | “Reciting the Buddhist Scriptures” random reward                               |
|        DEX 30% | Acc+5, DEX+3, Avoid+1 | Extra B                                                                                                                                     |                                                                                |
|        DEX 60% | Acc+2, DEX+1          | Maverick Y                                                                                                                                  | “The Secret Operation” random reward                                           |
|         HP 30% | MaxHP+30              | The Boss                                                                                                                                    |                                                                                |
| Magic Att. 30% | Matk+3, INT+3, Mdef+1 | Nameless Magic Monster                                                                                                                      |                                                                                |
| Magic Att. 60% | Matk+2, INT+1         | Jonin                                                                                                                                       |                                                                                |

#### For Shields

|            name | effect                   | dropped by                                                                                 | awarded by                           |
|----------------:|:-------------------------|:-------------------------------------------------------------------------------------------|:-------------------------------------|
|         DEF 10% | Wdef+5, Mdef+3, MaxHP+10 | Green Mushroom, Sheep, Cow, Black Bear Swordsman, Jr. Necki, Jiaoceng                      |                                      |
|         DEF 60% | Wdef+2, Mdef+1           | Censer, Ravana                                                                             | “The Secret Operation” random reward |
|          HP 30% | MaxHP+30                 | Censer                                                                                     |                                      |
|          HP 70% | MaxHP+15                 | Female Boss                                                                                |                                      |
|         LUK 30% | LUK+3                    | The Boss                                                                                   |                                      |
|  Magic Att. 30% | Matk+3, INT+2            | Ninto, Nibelung                                                                            |                                      |
|  Magic Att. 60% | Matk+2, INT+1            | Iruvata, Kacchuu Musha, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster      |                                      |
|  Magic Att. 70% | Matk+2, INT+1            | Prototype Lord                                                                             |                                      |
| Weapon Att. 10% | Watk+3, STR+2            | Genin                                                                                      |                                      |
| Weapon Att. 30% | Watk+3, STR+2            | Chunin                                                                                     |                                      |
| Weapon Att. 60% | Watk+2, STR+1            | Ashigaru, Kunoichi, Iruvata, Bergamot, Nibelung, Dunas (level 174), Nameless Magic Monster |                                      |
| Weapon Att. 70% | Watk+2, STR+1            | Bergamot                                                                                   |                                      |

#### For Shoes

|             name | effect                  | dropped by                                                     | awarded by                                                                |
|-----------------:|:------------------------|:---------------------------------------------------------------|:--------------------------------------------------------------------------|
| Avoidability 10% | Avoid+5, Acc+3, Speed+1 | Jiaoceng                                                       |                                                                           |
| Avoidability 30% | Avoid+5, Acc+3, Speed+1 | Lucida                                                         |                                                                           |
| Avoidability 60% | Avoid+2, Acc+1          | Rooster, Duck, Sheep                                           | “The Secret Operation” random reward                                      |
|         Jump 10% | Jump+5, DEX+3, Speed+1  | Orange Mushroom, Jiaoceng                                      |                                                                           |
|         Jump 30% | Jump+5, DEX+3, Speed+1  | Crow, Black Crow                                               | “Playing Around in Ninja Town”; “Sakura, the Kitty and the Orange Marble” |
|         Jump 60% | Jump+2, DEX+1           | Maverick A, Giant Centipede                                    | “The Secret Operation” random reward                                      |
|        Speed 10% | Speed+3                 | Jiaoceng                                                       |                                                                           |
|        Speed 30% | Speed+3                 | Female Boss                                                    |                                                                           |
|        Speed 60% | Speed+2                 | Water Goblin, Golden Giant, Mini Gold Martial Artist, Jiaoceng | “The Secret Operation” random reward                                      |
|        Speed 70% | Speed+2                 | Water Goblin, Black Crow                                       |                                                                           |

#### For Pet Equips

|       name | effect  | dropped by                | awarded by |
|-----------:|:--------|:--------------------------|:-----------|
|  Jump 100% | Jump+1  | Green Mushroom, Jr. Necki |            |
|  Speed 60% | Speed+2 | Green Mushroom, Cloud Fox |            |
| Speed 100% | Speed+1 | Green Mushroom            |            |

#### For Aufheben Circlet

|    name | effect | dropped by                                                                                             | awarded by |
|--------:|:-------|:-------------------------------------------------------------------------------------------------------|:-----------|
| STR 50% | STR+3  | Bergamot, Nibelung, Dunas (level 174), Dunas (level 176), Nameless Magic Monster, Core Blaze, Aufheben |            |
| DEX 50% | DEX+3  | Bergamot, Nibelung, Dunas (level 174), Dunas (level 176), Nameless Magic Monster, Core Blaze, Aufheben |            |
| INT 50% | INT+3  | Bergamot, Nibelung, Dunas (level 174), Dunas (level 176), Nameless Magic Monster, Core Blaze, Aufheben |            |
| LUK 50% | LUK+3  | Bergamot, Nibelung, Dunas (level 174), Dunas (level 176), Nameless Magic Monster, Core Blaze, Aufheben |            |

#### Clean Slate Scrolls

|                   name | effect                     | dropped by | awarded by |
|-----------------------:|:---------------------------|:-----------|:-----------|
| Clean Slate Scroll 20% | recovers lost upgrade slot | Core Blaze |            |
