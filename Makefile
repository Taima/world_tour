.PHONY: all
all: README.md

README.md: toc.md introduction.md rules.md legal.md
	rm -rf README.md
	cat toc.md >> README.md
	echo '' >> README.md
	cat introduction.md >> README.md
	echo '' >> README.md
	cat rules.md >> README.md
	echo '' >> README.md
	cat legal.md >> README.md
	echo '' >> README.md
