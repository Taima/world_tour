## Monsters

### Mushroom Shrine, A Desolate Cemetery, Vanished Village

| monster             | level |     hp |   exp | w att | m att | w def | m def | acc | avoid | elemental                      |
|:--------------------|------:|-------:|------:|------:|------:|------:|------:|----:|------:|:-------------------------------|
| Orange Mushroom     |     8 |     80 |    15 |    42 |     0 |     0 |     0 |  42 |     1 |                                |
| Green Mushroom      |    15 |    250 |    26 |    82 |     0 |    12 |    40 |  45 |     5 | immune to poison               |
| Blue Mushroom       |    20 |    350 |    32 |    90 |     0 |    10 |    60 |  55 |     7 |                                |
| Zombie Mushroom     |    24 |    500 |    42 |    95 |     0 |    20 |    30 |  50 |     8 | weak to holy                   |
| Crow                |    25 |    550 |    42 |    90 |     0 |    30 |    30 |  50 |    10 | weak to lightning              |
| Cloud Fox           |    30 |    920 |    55 |    98 |     0 |    80 |    70 |  60 |    12 | weak to fire                   |
| Fire Raccoon        |    30 |    900 |    55 |    95 |   105 |    85 |    65 |  65 |    12 | weak to ice                    |
| Paper Lantern Ghost |    40 |  1,920 |    93 |   120 |   145 |   140 |   160 | 150 |    25 | weak to ice; strong to fire    |
| Big Cloud Fox       |    45 |  2,600 |   110 |   145 |     0 |   125 |   175 | 180 |    20 | weak to lightning              |
| Samiho              |    56 |  4,500 |   185 |   165 |   180 |   150 |   190 | 135 |    28 | weak to fire and holy          |
| Coolie Zombie       |    57 |  4,500 |   190 |   165 |   185 |   165 |   180 | 135 |    25 | weak to holy; immune to poison |
| Dark Stone Golem    |    58 |  4,800 |   200 |   195 |     0 |   150 |   200 |  70 |    18 | weak to holy                   |
| Nightghost          |    60 |  7,100 |   220 |   165 |     0 |   200 |   220 | 160 |    25 | weak to holy, fire, and ice    |
| Water Goblin        |    60 |  7,100 |   220 |   190 |   250 |   200 |   230 | 190 |    22 | weak to ice; strong to fire    |
| Lucida              |    73 | 15,500 |   320 |   280 |   315 |   300 |   320 | 160 |    28 | weak to holy; immune to poison |
| Bain                |    90 | 45,000 | 1,800 |   425 |   465 |   835 |   505 | 195 |    38 | weak to ice; strong to fire    |
| Dreamy Ghost        |   100 | 68,000 | 3,200 |   480 |   540 |   810 |   580 | 210 |    34 | weak to holy; immune to poison |

### Showa Town, The Secret Spa, Hideout “Finance of Flaming Raccoon”

| monster     | level |     hp | exp | w att | m att | w def | m def | acc | avoid | elemental                   |
|:------------|------:|-------:|----:|------:|------:|------:|------:|----:|------:|:----------------------------|
| Jr. Cerebes |    43 |  2,300 | 102 |   132 |     0 |   290 |   150 | 155 |    25 |                             |
| Extra A     |    45 |  2,800 | 130 |   140 |     0 |   145 |   125 | 140 |    22 | weak to poison and fire     |
| Extra B     |    47 |  2,800 | 139 |   150 |     0 |   140 |   110 | 143 |    18 | weak to poison and fire     |
| Extra C     |    50 |  3,100 | 148 |   150 |     0 |   185 |   170 | 148 |    20 | weak to poison and fire     |
| Firebomb    |    51 |  3,600 | 142 |   145 |     0 |   255 |   205 | 155 |    28 | weak to ice; strong to fire |
| Red Slime   |    55 |  6,000 | 240 |   210 |   190 |   200 |   220 | 150 |    25 | weak to ice                 |
| Leader A    |    62 | 10,000 | 310 |   190 |     0 |   210 |   175 | 165 |    26 | weak to poison and fire     |
| Leader B    |    64 |  9,000 | 320 |   160 |   200 |   190 |   200 | 150 |    24 | weak to poison and fire     |
| Extra D     |    72 | 15,100 | 465 |   270 |   315 |   320 |   205 | 170 |    18 | weak to fire, holy, and ice |

### Ninja Castle

| monster  | level |     hp |   exp | w att | m att | w def | m def | acc | avoid | elemental                                                    |
|:---------|------:|-------:|------:|------:|------:|------:|------:|----:|------:|:-------------------------------------------------------------|
| Genin    |    25 |    600 |    75 |    95 |     0 |    20 |    35 |  90 |    14 | weak to poison and fire                                      |
| Ashigaru |    30 |    950 |   100 |   125 |     0 |    60 |    50 |  55 |     8 | weak to ice and poison                                       |
| Chunin   |    50 |  4,200 |   190 |   162 |     0 |   115 |   180 | 162 |    40 | weak to lightning and fire                                   |
| Kunoichi |    60 |  6,500 |   290 |   200 |   280 |   165 |   210 | 210 |    22 | weak to fire and holy                                        |
| Jonin    |    80 | 25,000 | 1,150 |   370 |   405 |   610 |   470 | 155 |    30 | weak to holy; immune to poison                               |
| Ninto    |   100 | 71,000 | 4,050 |   580 |   620 | 1,020 |   610 | 215 |    35 | weak to holy; strong to lightning and fire; immune to poison |

### China: Shanghai Wai-Tan

| monster     | level |    hp | exp | w att | m att | w def | m def | acc | avoid | elemental |
|:------------|------:|------:|----:|------:|------:|------:|------:|----:|------:|:----------|
| Rooster     |    20 |   340 |  33 |    85 |     0 |    10 |    20 |  55 |     7 |           |
| Duck        |    22 |   380 |  37 |    90 |     0 |    20 |    20 |  55 |     7 |           |
| Sheep       |    25 |   550 |  42 |    95 |     0 |    20 |    30 |  50 |     8 |           |
| Goat        |    30 |   900 |  58 |   100 |     0 |    40 |    40 |  65 |    12 |           |
| Cow         |    33 | 1,200 |  66 |   110 |     0 |    50 |    40 |  60 |    12 |           |
| Black Goat  |    35 | 1,250 |  71 |   120 |     0 |    60 |    50 |  65 |    12 |           |
| Black Sheep |    37 | 1,500 |  79 |   115 |   125 |   100 |   115 |  90 |    18 |           |
| Plow Ox     |    38 | 1,800 |  86 |   120 |     0 |   105 |   120 | 100 |    20 |           |

### China: Mount Song Town / Shaolin Temple

| monster                    | level |      hp |    exp | w att | m att | w def | m def | acc | avoid | elemental |
|:---------------------------|------:|--------:|-------:|------:|------:|------:|------:|----:|------:|:----------|
| Black Bear Swordsman       |    80 |  40,500 |  1,820 |   350 |   410 |   650 |   450 | 140 |    28 |           |
| White Tiger Swordsman      |    90 |  63,000 |  3,270 |   415 |   450 |   830 |   500 | 195 |    37 |           |
| Eagle Swordsman            |    95 |  78,000 |  4,400 |   450 |   500 |   835 |   540 | 200 |    37 |           |
| Female Thief               |    95 |  78,000 |  4,400 |   450 |   500 |   835 |   540 | 200 |    37 |           |
| Male Thief                 |    95 |  78,000 |  4,400 |   450 |   500 |   835 |   540 | 200 |    37 |           |
| Censer                     |   100 |  85,500 |  4,820 |   480 |   600 |   800 |   570 | 205 |    40 |           |
| Wooden Fish                |   105 | 102,000 |  5,865 |   500 |   550 |   750 |   680 | 205 |    38 |           |
| Bronze Staffman            |   120 | 135,000 |  7,550 |   450 |   450 |   950 |   800 | 230 |    43 |           |
| Mini Bronze Martial Artist |   120 | 135,000 |  7,550 |   450 |   450 |   950 |   800 | 230 |    43 |           |
| Silver Giant               |   125 | 225,000 | 12,435 |   510 |   510 | 1,100 | 1,000 | 250 |    45 |           |
| Silver Spearman            |   125 | 225,000 | 11,935 |   510 |   510 | 1,100 | 1,000 | 250 |    45 |           |
| Golden Giant               |   130 | 270,000 | 14,890 |   550 |   520 | 1,300 | 1,300 | 200 |    75 |           |
| Mini Gold Martial Artist   |   130 | 270,000 | 14,890 |   550 |   520 | 1,300 | 1,300 | 200 |    75 |           |

### Thailand: Floating Market

| monster       | level |    hp | exp | w att | m att | w def | m def | acc | avoid | elemental                   |
|:--------------|------:|------:|----:|------:|------:|------:|------:|----:|------:|:----------------------------|
| Frog          |     8 |    75 |  17 |    52 |     0 |     5 |    10 |  42 |     1 |                             |
| White Rooster |    15 |   230 |  28 |    82 |     0 |    11 |    40 |  45 |     5 |                             |
| Rooster       |    20 |   340 |  33 |    85 |     0 |    10 |    20 |  55 |     7 |                             |
| Jr. Necki     |    21 |   285 |  38 |   100 |     0 |    30 |    30 | 120 |    25 |                             |
| Yellow Lizard |    25 |   500 |  46 |    90 |     0 |    20 |    30 |  50 |     8 |                             |
| Toad          |    28 |   720 |  58 |   105 |     0 |    60 |    73 | 115 |    12 |                             |
| Ligator       |    32 | 1,200 |  60 |   110 |     0 |    45 |    40 |  70 |    12 | weak to fire; strong to ice |
| Red Lizard    |    40 | 1,920 |  95 |   124 |     0 |   110 |   130 |  65 |    15 |                             |
| Croco         |    52 | 3,800 | 170 |   172 |     0 |   120 |    80 |  80 |    20 |                             |
| Python        |    60 | 6,000 | 230 |   200 |   190 |   160 |   220 | 150 |    22 |                             |

### Thailand: Golden Temple / Monkey Temple / Ghost Cave

| monster             | level |     hp |   exp | w att | m att | w def | m def | acc | avoid | elemental                   |
|:--------------------|------:|-------:|------:|------:|------:|------:|------:|----:|------:|:----------------------------|
| White Baby Monkey   |    20 |    300 |    33 |    85 |     0 |    20 |    20 |  80 |    10 |                             |
| Wild Monkey         |    42 |  2,200 |   100 |   130 |   130 |   130 |   160 | 100 |    12 |                             |
| Mama Monkey         |    50 |  3,600 |   140 |   140 |   130 |   145 |   180 | 120 |    25 |                             |
| White Mama Monkey   |    65 | 11,000 |   255 |   182 |   270 |   170 |   245 | 110 |    24 | weak to fire; strong to ice |
| Blue Goblin         |    83 | 30,000 | 1,100 |   360 |     0 |   700 |   460 | 150 |    27 | weak to fire                |
| Red Goblin          |    90 | 40,000 | 1,800 |   410 |     0 |   820 |   480 | 195 |    35 | weak to ice                 |
| Strong Stone Goblin |    99 | 60,000 | 2,800 |   465 |   530 |   850 |   570 | 205 |    38 | weak to holy                |

### Neo Tokyo

| monster               | level |        hp |     exp | w att | m att | w def | m def | acc | avoid | elemental                                                  |
|:----------------------|------:|----------:|--------:|------:|------:|------:|------:|----:|------:|:-----------------------------------------------------------|
| Prototype Lord        |   115 |   190,000 |  10,500 |   600 |   300 |   850 |   950 | 250 |    20 | strong to holy; immune to poison                           |
| Afterlord             |   135 |   260,000 |  11,350 |   610 |   650 | 1,110 | 1,020 | 250 |    20 | strong to ice and holy; immune to poison                   |
| Overlord              |   137 |   150,000 |   7,100 |   320 |   420 |   920 |   750 | 280 |    42 | strong to fire, lightning, ice, and holy; immune to poison |
| Imperial Guard        |   143 | 9,430,000 |  11,500 |   640 |   640 | 1,500 | 1,000 | 270 |    47 | immune to poison                                           |
| Imperial Guard Type A |   143 | 9,430,000 | 172,500 |   640 |   640 | 1,500 | 1,000 | 270 |    47 | immune to poison                                           |
| Iruvata               |   143 |   280,000 |  13,175 |   700 |   700 | 1,210 |   910 | 270 |    45 | strong to holy; immune to poison                           |
| Maverick A            |   141 |   270,000 |  12,150 |   620 |   640 | 1,100 |   890 | 270 |    41 | strong to fire, ice, and holy; immune to poison            |
| Maverick B            |   143 |   820,000 |  13,500 |   640 |   640 | 1,110 |   850 | 270 |    43 | strong to holy; immune to poison                           |
| Maverick Form B       |   143 |   820,000 |       0 |   640 |   640 | 1,110 |   850 | 270 |    43 | strong to holy; immune to poison                           |
| Maverick S            |   143 |   820,000 |  13,500 |   640 |   640 | 1,110 |   850 | 270 |    43 | strong to holy; immune to poison                           |
| Maverick V            |   143 |   200,000 |  10,800 |   640 |   640 | 1,110 |   850 | 270 |    43 | strong to holy; immune to poison                           |
| Maverick Y            |   143 |   270,000 |  12,270 |   640 |   640 | 1,110 |   850 | 270 |    43 | strong to holy; immune to poison                           |

### Bosses

| monster                | level |          hp |        exp | w att | m att | w def | m def |   acc | avoid | elemental                                                          |
|:-----------------------|------:|------------:|-----------:|------:|------:|------:|------:|------:|------:|:-------------------------------------------------------------------|
| Tokyo stuff            |     1 |  82,000,000 |          0 |     0 |     0 |     0 |     0 |     0 |     0 |                                                                    |
| Giant Centipede        |    50 |      15,000 |        425 |   320 |   340 |   300 |   150 |   150 |    27 |                                                                    |
| Blue Mushmom           |    90 |     200,000 |     10,000 |   450 |   540 |   810 |   520 |   220 |    64 | weak to fire                                                       |
| Male Boss              |    95 |      85,000 |      5,500 |   750 |   600 |   720 |   680 |   175 |    35 | weak to fire and lightning                                         |
| Black Crow             |   115 |  35,000,000 |  1,780,000 | 1,000 |   800 | 1,400 | 1,300 |   250 |    39 | weak to lightning                                                  |
| Kacchuu Musha          |   120 |  78,000,000 |  3,680,000 | 1,150 | 1,080 | 3,200 | 1,200 |   270 |    42 | weak to lightning, holy, and ice; strong to fire; immune to poison |
| Ravana                 |   120 |  35,000,000 |  1,500,000 |   800 |   800 | 1,150 | 1,400 |   247 |    15 |                                                                    |
| Female Boss            |   130 |  75,000,000 |  3,900,000 | 1,200 |   830 | 3,000 | 1,250 |   280 |    43 | weak to lightning and holy                                         |
| Toad                   |   130 |      98,000 |      6,200 |   620 |     0 |   880 |   820 |   205 |    40 | weak to lightning; strong to fire                                  |
| Nameless Magic Monster |   131 |      30,000 |     26,850 |   665 |   735 | 1,020 | 1,110 |   232 |    47 | weak to lightning; strong to ice                                   |
| Jiaoceng               |   140 |  15,000,000 |    200,000 |   700 |   700 |   720 |   720 |   160 |    20 |                                                                    |
| Jiaoceng               |   150 |  80,000,000 |  5,000,000 |   800 |   800 |   980 |   980 |   220 |    27 |                                                                    |
| Attack                 |   160 |   9,999,999 |          0 |   500 |   500 |   500 |   500 |    50 |    50 |                                                                    |
| Chandellier            |   165 |     100,000 |          0 | 1,999 |   500 |   700 |   700 | 1,000 |   200 | immune to fire, lightning, ice, poison, and holy                   |
| Bergamot               |   168 | 145,000,000 |  3,100,000 | 1,150 | 1,020 | 2,200 | 1,350 |   270 |    35 | weak to ice and lightning; strong to fire                          |
| Bergamot               |   168 |  85,000,000 |  4,200,000 | 1,030 |   960 | 1,450 | 1,100 |   260 |    32 | weak to fire                                                       |
| Bergamot               |   168 |  70,000,000 |  5,300,000 | 1,250 | 1,280 | 1,000 |   800 |   250 |    28 |                                                                    |
| Nibelung               |   168 | 172,000,000 |  3,100,000 |   800 | 1,000 | 1,430 | 1,100 |   290 |    33 | weak to lightning and ice; strong to fire                          |
| Nibelung               |   168 |  95,000,000 |  4,200,000 |   800 | 1,000 | 1,230 | 1,080 |   270 |    30 | weak to lightning and ice; strong to fire                          |
| Nibelung               |   168 |  82,000,000 |  5,300,000 | 1,180 | 1,280 | 1,010 |   910 |   269 |    28 | weak to lightning and ice; strong to fire                          |
| Dunas                  |   174 |  50,000,000 |  1,200,000 | 1,000 | 1,050 | 1,000 | 1,500 |   300 |    22 |                                                                    |
| Dunas                  |   174 | 230,000,000 |  9,500,000 |   450 | 1,000 |   520 | 1,450 |   280 |    21 |                                                                    |
| Dunas Type D           |   174 | 100,000,000 |  2,000,000 | 1,000 |   700 | 1,999 |   700 |   400 |    35 | weak to lightning; immune to poison and holy                       |
| Dunas Unit             |   174 | 100,000,000 |  2,000,000 | 1,000 |   700 | 1,999 |   700 |   400 |    35 | weak to lightning                                                  |
| Nameless Magic Monster |   174 | 430,000,000 | 10,300,000 |   920 |   980 |   950 | 1,050 |   290 |    40 | weak to lightning; strong to ice                                   |
| Cursed Aufheben        |   175 |  90,000,000 |  1,500,000 |   400 |   900 |   600 | 1,200 |   350 |    25 | weak to lightning and poison; strong to ice and fire               |
| Summoned Aufheben      |   175 |  50,000,000 |  2,750,000 | 1,000 | 1,000 |   900 |   900 |   300 |    20 | weak to holy; strong to fire and ice                               |
| Summoned Aufheben      |   175 | 100,000,000 |  6,000,000 | 1,000 | 1,000 |   900 |   900 |   300 |    20 | weak to holy; strong to fire and ice                               |
| The Boss               |   175 | 150,000,000 | 17,500,000 | 1,600 | 1,350 | 5,000 | 4,300 |   300 |    60 | weak to fire                                                       |
| Dunas                  |   176 | 250,000,000 |  6,000,000 | 1,200 | 1,200 | 1,200 | 1,500 |   300 |    23 |                                                                    |
| Core Blaze             |   178 | 300,000,000 | 18,000,000 | 1,700 | 1,450 | 1,999 | 1,999 |   350 |    20 | weak to fire and lightning                                         |
| Aufheben               |   180 | 520,000,000 | 15,450,000 | 1,550 | 1,200 | 1,700 | 1,930 |   270 |    55 | weak to lightning and poison; strong to ice and fire               |
| Castellan              |   180 | 620,000,000 | 22,550,000 | 1,480 | 1,320 | 4,200 | 2,600 |   320 |    42 | weak to poison                                                     |
| Castellan Toad         |   180 | 450,000,000 | 25,300,000 | 1,520 | 1,360 | 4,300 | 2,900 |   330 |    45 |                                                                    |

#### Notes

-   Item drops have been excluded from this monster from the MapleLegends Library: Tokyo Stuff

### Monster Cards (76 sets in total)

#### Mushroom Shrine, A Desolate Cemetery, Vanished Village (17 sets)

-   Orange Mushroom Card
-   Green Mushroom Card
-   Blue Mushroom Card
-   Cloud Fox Card
-   Lucida Card
-   Crow Card
-   Zombie Mushroom Card
-   Dark Stone Golem Card
-   Fire Raccoon Card
-   Paper Lantern Ghost Card
-   Big Cloud Fox Card “Big Cloud Fox”
-   Water Goblin Card
-   Samiho Card “Tri-Tailed Fox Card”
-   Dreamy Ghost Card
-   Nightghost Card
-   Coolie Zombie Card
-   Bain Card

#### Showa Town, The Secret Spa, Hideout, Finance of Flaming Raccoon (8 sets)

-   Extra A Card
-   Extra B Card
-   Extra C Card
-   Extra D Card
-   Leader A Card
-   Leader B Card
-   Jr. Cerebes Card
-   Firebomb Card

#### Ninja Castle (5 sets)

-   Genin Card
-   Ashigaru Card
-   Chunin Card
-   Kunoichi Card
-   Ninto Card

#### China: Shanghai Wai-Tan (8 sets)

-   Rooster Card (also in Taiwan: Floating Market)
-   Duck Card
-   Sheep Card
-   Goat Card
-   Cow Card
-   Black Goat Card
-   Black Sheep Card
-   Plow Ox Card

#### China: Mount Song Town / Shaolin Temple (11 sets)

-   Eagle Swordsman Card
-   Female Thief Card
-   Male Thief Card
-   Censer Card
-   Wooden Fish Card
-   Bronze Staffman Card “Bronze Spearman Card”
-   Mini Bronze Martial Artist Card “Bronze Martial Aritist Card”
-   Silver Giant Card
-   Silver Spearman Card
-   Golden Giant Card
-   Mini Gold Martial Artist Card “Gold Martial Artist Card”

#### Thailand: Floating Market (4 sets)

-   Rooster Card (also in China: Shanghai Wai-tan)
-   Jr. Necki Card
-   Ligator Card
-   Croco Card

#### Neo Tokyo (9 sets)

-   Prototype Lord Card
-   Afterlord Card
-   Overlord Card
-   Maverick A Card
-   Imperial Guard Card
-   Iruvata Card
-   Maverick B Card
-   Maverick V Card
-   Maverick Y Card

#### Bosses (15 sets)

-   Giant Centipede Card
-   Male Boss Card
-   Black Crow Card
-   Female Boss Card
-   Jiaoceng Card (Blue)
-   Jiaoceng Card (Red)
-   Bergamot Card
-   Nibelung Card
-   Dunas Card
-   Dunas 2 Card
-   Nameless Magic Monster Card
-   The Boss Card
-   Core Blaze Card
-   Aufheben Card
-   Castellan Card
