## 4th Job Skills

### Common

| Skill         | Max Level | Previous Max Level |
|:--------------|----------:|-------------------:|
| Hero’s Will   |         5 |                  5 |
| Maple Warrior |        10 |                 30 |

-   Hero’s Will
    -   Skill Book quest available for all 4th jobs
-   Maple Warrior
    -   Skill Book quest available for all 4th jobs

### Warrior

| Skill          | Max Level | Previous Max Level |
|:---------------|----------:|-------------------:|
| Achilles       |        20 |                 30 |
| Guardian       |        20 |                 30 |
| Monster Magnet |        10 |                 30 |
| Rush           |        20 |                 30 |
| Power Stance   |        20 |                 30 |

-   Achilles
    -   Default skill for all 4th job Warriors
    -   Obtainable \[Mastery Book 20\] Achilles
-   Guardian
    -   Skill Book quest available for Hero and Paladin
    -   Requires having a shield equipped to begin the quest
    -   Obtainable \[Mastery Book 20\] Guardian
-   Monster Magnet
    -   Default skill for all 4th job Warriors
-   Rush
    -   Skill Book quest available for all 4th job Warriors
    -   Requires \[Storybook\] Indecipherable Book
    -   Requires assistance from another 4th job Warrior
    -   Obtainable \[Mastery Book 20\] Rush
-   Power Stance
    -   Skill Book quest available for all 4th job Warriors
    -   Requires \[Storybook\] Crimson Balrog’s Proposal
    -   Requires El Nath PQ participation
    -   Obtainable \[Mastery Book 20\] Power Stance

#### Dark Knight

| Skill                | Max Level | Previous Max Level |
|:---------------------|----------:|-------------------:|
| Aura of the Beholder |        25 |                 25 |
| Beholder             |        10 |                 10 |
| Berserk              |        10 |                 30 |
| Hex of the Beholder  |        25 |                 25 |

-   Aura of the Beholder
    -   Skill Book quest available
    -   Increase skill max level via quest
-   Beholder
    -   Default skill for all Dark Knights
-   Berserk
    -   Skill Book quest available
-   Hex of the Beholder
    -   Skill Book quest available
    -   Increase skill max level via quest

#### Hero

| Skill                 | Max Level | Previous Max Level |
|:----------------------|----------:|-------------------:|
| Advanced Combo Attack |        10 |                 30 |
| Brandish              |        10 |                 30 |
| Enrage                |        10 |                 30 |

-   Advanced Combo Attack
    -   Skill Book obtainable
    -   Requires Zakum boss solo
    -   Obtainable \[Mastery Book 30\] Advanced Combo Attack
-   Brandish
    -   Default skill for all Heroes
-   Enrage
    -   Skill Book quest available

#### Paladin

| Skill           | Max Level | Previous Max Level |
|:----------------|----------:|-------------------:|
| Advanced Charge |        10 |                 10 |
| Blast           |        20 |                 30 |
| Divine Charge   |        20 |                 20 |
| Heaven’s Hammer |        10 |                 30 |
| Holy Charge     |        20 |                 20 |

-   Advanced Charge
    -   Skill Book obtainable
    -   Requires Zakum boss solo
-   Blast
    -   Default skill for all Paladins
    -   Obtainable \[Mastery Book 20\] Blast
-   Divine Charge (for Blunt Weapons)
    -   Skill Book quest available
    -   Requires Horntail PQ participation
    -   Obtainable \[Mastery Book 20\] Divine Charge
-   Heaven’s Hammer
    -   Skill Book quest available
-   Holy Charge (for Swords)
    -   Skill Book quest available
    -   Requires Horntail PQ participation
    -   Obtainable \[Mastery Book 20\] Holy Charge

### Magician

| Skill           | Max Level | Previous Max Level |
|:----------------|----------:|-------------------:|
| Big Bang        |        10 |                 30 |
| Infinity        |        10 |                 30 |
| Mana Reflection |        20 |                 30 |

-   Big Bang
    -   Default skill for all 4th job Magicians
-   Infinity
    -   Skill Book available for all 4th job Magicians
    -   Requires El Nath PQ participation
-   Mana Reflection
    -   Default skill for all 4th job Magicians
    -   Obtainable \[Mastery Book 20\] Mana Reflection

#### Arch Mage(Fire, Poison)

| Skill         | Max Level | Previous Max Level |
|:--------------|----------:|-------------------:|
| Elquines      |        10 |                 30 |
| Fire Demon    |        20 |                 30 |
| Meteor Shower |        10 |                 30 |
| Paralyze      |        20 |                 30 |

-   Elquines
    -   Skill Book quest available
    -   Prerequisite: Fire Demon skill unlocked
    -   Requires assistance from another Arch Mage
-   Fire Demon
    -   Skill Book quest available
    -   Requires \[Storybook\] Burning Book of Fire
    -   Obtainable \[Mastery Book 20\] Fire Demon
-   Meteor Shower
    -   Skill Book quest available
    -   Requires Eye of Fire from Zakum prequest
-   Paralyze
    -   Default skill for (F/P) Arch Mages
    -   Obtainable \[Mastery Book 20\] Paralyze

#### Arch Mage(Ice, Lightning)

| Skill           | Max Level | Previous Max Level |
|:----------------|----------:|-------------------:|
| Blizzard        |        10 |                 30 |
| Chain Lightning |        20 |                 30 |
| Ice Demon       |        20 |                 30 |
| Ifrit           |        20 |                 30 |

-   Blizzard
    -   Skill Book quest available
-   Chain Lightning
    -   Default skill for all (I/L) Arch Mages
    -   Obtainable \[Mastery Book 20\] Chain Lightning
-   Ice Demon
    -   Skill Book quest available
    -   Requires \[Storybook\] Frozen Book of Ice
    -   Obtainable \[Mastery Book 20\] Ice Demon
-   Ifrit
    -   Skill Book quest available
    -   Prerequisite: Ice Demon skill unlocked
    -   Requires assistance from another Arch Mage
    -   Obtainable \[Mastery Book 20\] Ifrit

#### Bishop

| Skill        | Max Level | Previous Max Level |
|:-------------|----------:|-------------------:|
| Angel Ray    |        20 |                 30 |
| Bahamut      |        30 |                 30 |
| Genesis      |        10 |                 30 |
| Holy Shield  |        20 |                 30 |
| Resurrection |        10 |                 10 |

-   Angel Ray
    -   Skill Book obtainable
    -   Requires Zakum boss solo
    -   Obtainable \[Mastery Book 20\] Angel Ray
-   Bahamut
    -   Skill Book quest available
    -   Increase skill max level via quest
-   Genesis
    -   Skill Book quest available
    -   Requires Horntail PQ participation
-   Holy Shield
    -   Default skill for all Bishops
    -   Obtainable \[Mastery Book 20\] Holy Shield
-   Resurrection
    -   Skill Book quest available
    -   Requires \[Storybook\] Black Book

### Bowman

| Skill           | Max Level | Previous Max Level |
|:----------------|----------:|-------------------:|
| Dragon’s Breath |        20 |                 30 |
| Sharp Eyes      |        10 |                 30 |

-   Dragon’s Breath
    -   Skill Book quest available
    -   Requires El Nath PQ participation
    -   Obtainable \[Mastery Book 20\] Dragon’s Breath
-   Sharp Eyes
    -   Default skill for all 4th job Bowmen

#### Bow Master

| Skill       | Max Level | Previous Max Level |
|:------------|----------:|-------------------:|
| Bow Expert  |        20 |                 30 |
| Concentrate |        10 |                 30 |
| Hamstring   |        30 |                 30 |
| Hurricane   |        10 |                 30 |
| Phoenix     |        20 |                 30 |

-   Bow Expert
    -   Default skill for all Bow Masters
    -   Obtainable \[Mastery Book 20\] Bow Expert
-   Concentrate
    -   Skill Book quest available
    -   Requires \[Storybook\] Ancient Book
-   Hamstring
    -   Default skill for all Bow Masters
    -   Obtainable \[Mastery Book 20\] Hamstring Shot
    -   Obtainable \[Mastery Book 30\] Hamstring Shot
-   Hurricane
    -   Skill Book quest available
-   Phoenix
    -   Skill Book quest available
    -   Obtainable \[Mastery Book 20\] Phoenix

#### Marksman

| Skill          | Max Level | Previous Max Level |
|:---------------|----------:|-------------------:|
| Blind          |        30 |                 30 |
| Frostprey      |        20 |                 30 |
| Marksman Boost |        20 |                 30 |
| Piercing Arrow |        20 |                 30 |
| Snipe          |        10 |                 30 |

-   Blind
    -   Default skill for all Marksmen
    -   Obtainable \[Mastery Book 20\] Blind
    -   Obtainable \[Mastery Book 30\] Blind
-   Frostprey
    -   Skill Book quest available
    -   Obtainable \[Mastery Book 20\] Frostprey
-   Marksman Boost
    -   Default skill for all Marksmen
    -   Obtainable \[Mastery Book 20\] Marksman Boost
-   Piercing Arrow
    -   Skill Book quest available
    -   Obtainable \[Mastery Book 20\] Piercing Arrow
-   Snipe
    -   Skill Book quest available
    -   Requires \[Storybook\] Ancient Book

### Thief

| Skill                       | Max Level | Previous Max Level |
|:----------------------------|----------:|-------------------:|
| Ninja Ambush                |        10 |                 30 |
| Shadow Shifter              |        10 |                 30 |
| Taunt                       |        10 |                 30 |
| Venomous Star/Venomous Stab |        20 |                 30 |

-   Ninja Ambush
    -   Skill Book quest available
-   Shadow Shifter
    -   Default skill for all 4th job Thieves
-   Taunt
    -   Skill Book quest available
    -   Requires El Nath PQ participation
-   Venomous Stab and Venomous Star
    -   Default skill for all 4th job Thieves
    -   Obtainable \[Mastery Book 20\] Venomous Star/Venomous Stab

#### Night Lord

| Skill        | Max Level | Previous Max Level |
|:-------------|----------:|-------------------:|
| Ninja Storm  |        20 |                 30 |
| Shadow Stars |        10 |                 30 |
| Triple Throw |        10 |                 30 |

-   Ninja Storm
    -   Skill Book quest available
    -   Requires slaying Boogie summons during a Zakum boss solo
    -   Obtainable \[Mastery Book 20\] Ninja Storm
-   Shadow Stars
    -   Default skill for all Night Lords
-   Triple Throw
    -   Skill Book obtainable
    -   Requires Zakum boss solo

#### Shadower

| Skill          | Max Level | Previous Max Level |
|:---------------|----------:|-------------------:|
| Assassinate    |        20 |                 30 |
| Boomerang Step |        20 |                 30 |
| Smokescreen    |        10 |                 30 |

-   Assassinate
    -   Skill Book quest available
    -   Obtainable \[Mastery Book 20\] Assassinate
-   Boomerang Step
    -   Default skill for all Shadowers
    -   Obtainable \[Mastery Book 20\] Boomerang Step
-   Smokescreen
    -   Skill Book quest available
    -   Requires \[Storybook\] Formula for Black Cloud

### Pirate

#### Buccaneer

| Skill                | Max Level | Previous Max Level |
|:---------------------|----------:|-------------------:|
| Barrage              |        10 |                 30 |
| Demolition           |        10 |                 30 |
| Dragon Strike        |        10 |                 30 |
| Energy Orb           |        10 |                 30 |
| Snatch               |        10 |                 30 |
| Speed Infusion       |        10 |                 20 |
| Super Transformation |        10 |                 20 |
| Time Leap            |        10 |                 20 |

-   Barrage
    -   Default skill for Buccaneers
-   Demolition
    -   Skill Book quest available
    -   Prerequisite: Super Transformation skill unlocked
-   Dragon Strike
    -   Default skill for Buccaneers
-   Energy Orb
    -   Default skill for Buccaneers
-   Snatch
    -   Skill Book quest available
    -   Prerequisite: Super Transformation skill unlocked
-   Speed Infusion
    -   Default skill for Buccaneers
-   Super Transformation
    -   Skill Book quest available
-   Time Leap
    -   Skill Book quest available

#### Corsair

| Skill               | Max Level | Previous Max Level |
|:--------------------|----------:|-------------------:|
| Air Strike          |        10 |                 30 |
| Battleship          |        10 |                 10 |
| Battleship Cannon   |        10 |                 30 |
| Battleship Torpedo  |        10 |                 30 |
| Bullseye            |        10 |                 20 |
| Elemental Boost     |        10 |                 30 |
| Hypnotize           |        10 |                 20 |
| Rapid Fire          |        10 |                 30 |
| Wrath of the Octopi |        10 |                 20 |

-   Air Strike
    -   Skill Book quest available
-   Battleship
    -   Skill Book quest available
-   Battleship Cannon
    -   Skill Book quest available
-   Battleship Torpedo
    -   Skill Book quest available
-   Bullseye
    -   Default skill for Corsairs
-   Elemental Boost
    -   Default skill for Corsairs
-   Hypnotize
    -   Skill Book quest available
-   Rapid Fire
    -   Default skill for Corsairs
-   Wrath of the Octopi
    -   Default skill for Corsairs

### Skill Book quest information available at https://mapleroyals.com/forum/threads/4th-job-skill-mastery-book-guide.16552/
