## Introduction

![Spinel](./images/spinel_world_tour.png)

“If you’re tired of the monotonous daily life, how about going out for a change? There’s nothing quite like soaking up a new culture, and learning something new by the minute! It’s time for you to get out and travel. We, at the Maple Travel Agency recommend that you go on a World Tour!”

### 真・国際観光無双・ザワールドツアー

しん・こくさいかんこうむそう・ザワールドツアー

“True・International Tourism Unparalleled・THE WORLD TOUR”

#### The World Tour Challenge

-   A special area-locked gameplay challenge for MapleStory; explore a variety of travel destinations in the Orient and never leave!
-   For a small fee, the Maple Travel Agency will accomodate new Tourists on the trip of a lifetime to the Far East. The environment may be exceedingly hostile for a vacation destination, but in time you’ll hone your abilities and gain the trust of the locals. Establish your place in a different society, culture, and even time period!
-   This challenge is based on the MapleLegends (GMS v62-based) private server implementation of the World Tour areas.
-   Encompasses all of Spinel’s World Tour destinations:
    -   Shanghai of China
    -   Floating Market of Thailand
    -   Mushroom Shrine of Japan
    -   Shaolin Temple of China
    -   Golden Temple of Thailand

#### Notable Features of the World Tour Challenge

-   Hardcore grinding for equipment upgrades is heavily encouraged
-   You can use the Gachapon! Yes, just like in Japan
-   Utilize the Showa Exchange to its full potential
-   Several area bosses, mostly located in Japan
-   Some expedition bosses including Ravana and those in Neo Tokyo
-   Collect all of the items based on those in the Romance of the Three Kingdoms
-   Beautiful views of the Shanghai skyline
-   Terrifying encounters with Yokai
-   Shining gold Buddhist temples hidden deep in the thick jungle
-   A laid-back experience away from the pressures of Maple World
-   Contains content all the way up to the traditional “end game” of MapleStory, while keeping in line with the theme of the challenge

### Is it fun? Is it balanced?

-   World Tour area-lock is a niche within a niche, and I don’t expect many players to want to take part. The challenge is all about grinding hard for your own equipment and scrolls, exploring sometimes deeply underutilized maps and monsters, and appreciating some of the cultural references present in these cute, miniaturized versions of real life areas and their local folklore. Slide on your wooden geta and clack down the streets towards a new adventure.
-   Keep in mind, this challenge is unlikely to be very balanced. Depending on your job of choice, your character could face many levels in between equipment upgrades. You could have to grind low EXP monsters to obtain the scrolls you need, if they’re even available at all. Many Mastery Books and even some Skill Books aren’t obtainable, so all characters will in some way have their progression cut short compared to their vanilla counterparts in Maple World. Is the end game of Neo Tokyo even possible for these characters? Honestly, I’m too casual of a player to know. Look carefully at the available equipment, scrolls, and skills available before choosing your job!
