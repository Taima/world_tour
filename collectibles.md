## Collectibles

### Chairs

| name                      | description                                                                                       | dropped by        |
| :------------------------ | :------------------------------------------------------------------------------------------------ | :---------------- |
| Nibelung Battleship Chair | A futuristic chair that people in Neo City ride. It recovers 40 HP every 10 seconds.              | Nibelung          |
| Dunas Jet Chair           | A futuristic chair that people in Neo City ride. It recovers 40 HP every 10 seconds.              | Dunas (level 176) |
| Rowdy Chair               | Catch your breath and relax by sitting on this chair to recover 50 HP and 50 MP every 10 seconds. | Castellan Toad    |
| Toad Chair                | Catch your breath and relax by sitting on this chair to recover 50 HP and 50 MP every 10 seconds. | Castellan Toad    |
