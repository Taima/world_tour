## Quests

-   Warning: this page contains quest step and reward spoilers!

### China

#### Trading with Owner Yeo

-   Trading with Owner Yeo 1 (Over Level 10)
    -   China Quest
    -   Item(s) Needed:
        -   Chicken Feet x 100
    -   NPC(s) Involved:
        -   Owner Yeo
    -   Procedures:
        -   Talk to Owner Yeo
        -   Gather 100 Chicken Feet from Rooster
        -   Return to Owner Yeo
    -   Reward(s):
        -   Fruit Stick x 5
    -   Remark(s):
        -   Repeatable quest; rewards Fruit Stick x 5
-   Trading with Owner Yeo 2 (Over Level 10)
    -   China Quest
    -   Item(s) Needed:
        -   Duck Egg x 100
    -   NPC(s) Involved:
        -   Owner Yeo
    -   Procedures:
        -   Talk to Owner Yeo
        -   Gather 100 Duck Eggs from Duck
        -   Return to Owner Yeo
    -   Reward(s):
        -   Corn Stick x 10
    -   Remark(s):
        -   Repeatable quest; rewards Corn Stick x 10

#### Photographer Cho’s Request

-   Photographer Cho’s Request (Over Level 30)
    -   China Quest
    -   Item(s) Needed:
        -   Tripod
    -   NPC(s) Involved:
        -   Cho the photographer
        -   Mr. Yang
    -   Procedures:
        -   Talk to Cho the photographer
        -   Complete the quest “Help Mr. Yang Farm”
        -   Obtain Tripod from Mr. Yang
        -   Return to Cho the photographer
    -   Reward(s):
        -   1,800 exp

#### Help Mr. Yang Farm

-   Help Mr. Yang Farm
    -   China Quest
    -   Item(s) Needed:
        -   Plow x 100
    -   NPC(s) Involved:
        -   Mr. Yang
    -   Procedures:
        -   Talk to Mr. Yang
        -   Gather 100 Plows from Plow Ox
        -   Return to Mr. Yang
    -   Reward(s):
        -   6,500 exp
        -   Tripod
    -   Remark(s):
        -   Quest only available after starting “Photographer Cho’s Request”

#### Enraged Livestock

-   Enraged Livestock (Over Level 45)
    -   China Quest
    -   Item(s) Needed:
        -   Sheep Skin x 100
        -   White Horn x 100
    -   NPC(s) Involved:
        -   Mr. Yang
    -   Procedures:
        -   Talk to Mr. Yang
        -   Gather 100 Sheep Skins from Sheep and 100 White Horns from Goat
        -   Return to Mr. Yang
    -   Reward(s):
        -   11,000 exp

#### Mr. Yang’s Introduction

-   Mr. Yang’s Introduction (Over Level 45)
    -   China Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Mr. Yang
        -   Chief Officer Chu
    -   Procedures:
        -   Talk to Mr. Yang
        -   Bring the Recommendation Letter to Chief Officer Chu
    -   Reward(s):
        -   500 exp

#### Proof of Qualification

-   Proof of Qualification (Over Level 45)
    -   China Quest
    -   Item(s) Needed:
        -   Black Horn x 100
        -   Nose Ring x 100
    -   NPC(s) Involved:
        -   Chief Officer Chu
    -   Procedures:
        -   Talk to Chief Officer Chu
        -   Gather 100 Black Horns from Black Goat and 100 Nose Rings from Cow
        -   Return to Chief Officer Chu
    -   Reward(s):
        -   7,000 exp

#### The Secret Operation

-   The Secret Operation (Over Level 45)
    -   China Quest
    -   Item(s) Needed:
        -   Centipede Red Marble
    -   NPC(s) Involved:
        -   Chief Officer Chu
        -   Officer Lim
        -   Officer Chung
        -   Officer Huh
        -   Officer Kang
    -   Procedures:
        -   Talk to Chief Officer Chu and receive Shanghai Permit
        -   Bring the Shanghai Permit to Officer Lim to enter The Land of Black Sheep
        -   Complete “A Request from Officer Chung” to allow entrance to The Path
        -   Complete the puzzle to access Scary Valley
        -   Defeat Giant Centipede and gather Centipede Red Marble
        -   Return to Chief Officer Chu
    -   Reward(s):
        -   17,500 exp
    -   Random Reward(s):
        -   Scroll for Helmet for DEF 60% x 1
        -   Scroll for Helmet for HP 60% x 1
        -   Scroll for Earring for INT 60% x 1
        -   Scroll for Topwear for DEF 60% x 1
        -   Scroll for Overall Armor for DEX 60% x 1
        -   Scroll for Overall Armor for DEF 60% x 1
        -   Scroll for Bottomwear for DEF 60% x 1
        -   Scroll for Shoes for Avoidability 60% x 1
        -   Scroll for Shoes for Jump 60% x 1
        -   Scroll for Shoes for Speed 60% x 1
        -   Scroll for Gloves for DEX 60% x 1
        -   Scroll for Cape for Magic Def. 60% x 1
        -   Scroll for Cape for Weapon Def. 60% x 1
        -   Scroll for Shield for DEF 60% x 1
        -   Scroll for Gloves for ATT 60% x 1
        -   Steely Throwing-Knives x 1
        -   Blue Moon x 1
        -   Red Adventurer Cape x 1

#### A Request from Officer Chung

-   A Request from Officer Chung
    -   China Quest
    -   Item(s) Needed:
        -   Shanghai Permit
        -   Black Fur x 50
    -   NPC(s) Involved:
        -   Officer Chung
    -   Procedures:
        -   Talk to Officer Chung
        -   Gather 50 Black Furs from Black Sheep
    -   Reward(s):
        -   3,500 exp

#### Mayor’s Request

-   Mayor’s Request (At least at 3rd job advancement, Over Level 80)
    -   China Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Elder Jung
    -   Procedures:
        -   Talk to Elder Jung
        -   Eliminate 100 Black Bear Swordsman and 50 White Tiger Swordsman
        -   Return to Elder Jung
    -   Reward(s):
        -   40,000 exp
        -   Power Elixir x 10

#### Votive

-   Votive (At least at 3rd job advancement, Over Level 80)
    -   China Quest
    -   Item(s) Needed:
        -   Egg x 50
        -   Mana Elixir Pill x 20
        -   Holy Water x 10
    -   NPC(s) Involved:
        -   Hermit Qingwa
    -   Procedures:
        -   Talk to Hermit Qingwa
        -   Gather 50 Eggs, 20 Mana Elixir Pills, and 10 Holy Waters which can all be found at the Mount Song general store
        -   Return to Hermit Qingwa
    -   Reward(s):
        -   40,000 exp
        -   100,000 Mesos

#### The Door to Bai Shan

-   The Door to Bai Shan (At least at 3rd job advancement, Over Level
    90) <!-- end list -->
        <!-- end list -->
        <!-- end list -->
        <!-- end list -->

    -   China Quest
    -   Item(s) Needed:
        -   Eagle Swordsman’s Feather x 30
        -   Snitch’s Gauntlet x 20
        -   Thief’s Leg Band x 10
        -   Thief’s Wrist Guard x 50
        -   White Tiger’s Tail x 40
    -   NPC(s) Involved:
        -   Monk Zhi Ke
    -   Procedures:
        -   Talk to Monk Zhi Ke
        -   Gather 30 Eagle Swordsman’s Feather, 20 Snitch’s Gauntlet, 10 Thief’s Leg Band, 50 Thief’s Wrist Guard, and 40 White Tiger’s Tail from monsters outside of Shaolin Temple
        -   Return to Monk Zhi Ke
    -   Reward(s):
        -   150,000 exp
        -   1 Fame

#### A Search for Brother

-   A Search for Brother 1 (At least at 3rd job advancement, Over Level
    100) <!-- end list -->
         <!-- end list -->
         <!-- end list -->
         <!-- end list -->

    -   China Quest
    -   Item(s) Needed:
        -   Xu Zhu’s Letter
    -   NPC(s) Involved:
        -   Xu Zhu
    -   Procedures:
        -   Talk to Xu Zhu
        -   Deliver Xu Zhu’s Letter to his brother Guo Qing at China: Mountainside
    -   Reward(s):
        -   25,000 exp
-   A Search for Brother 2
    -   China Quest
    -   Item(s) Needed:
        -   Gao Qing’s Letter
    -   NPC(s) Involved:
        -   Guo Qing
    -   Procedures:
        -   Talk to Guo Qing at China: Mountside
        -   Deliver Gao Qing’s Letter to Xu Zhu
    -   Reward(s):
        -   50,000 exp
        -   1 Fame
        -   Elixir x 10

#### Buddha beads

-   Buddha beads (4th job advancement, Over Level 120)
    -   China Quest
    -   Pre-requisite: Mayor’s Request
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Wise Chief Priest
        -   Master Hai Feng
    -   Procedures:
        -   Talk to Wise Chief Priest
        -   Talk to Master Hai Feng for more information
    -   Reward(s):
        -   5,000 exp
-   Buddha beads 2
    -   China Quest
    -   Item(s) Needed:
        -   Jade Bead x 100
    -   NPC(s) Involved:
        -   Master Hai Feng
    -   Procedures:
        -   Talk to Master Hai Feng
        -   Gather 100 Jade Beads from Censer
    -   Reward(s):
        -   80,000 exp
        -   2 Fame
        -   Guardian Spirit Bun x 3

#### The Secret of the Sutra Depository

-   The Secret of the Sutra Depository 1 (4th job advancement, Over Level 120)
    -   China Quest
    -   Item(s) Needed:
        -   Bronze Martial Artist’s Heart x 50
    -   NPC(s) Involved:
        -   Janitor Monk
    -   Procedures:
        -   Talk to Janitor Monk
        -   Eliminate 100 Mini Bronze Martial Artist and gather 50 Bronze Martial Artist’s Hearts
        -   Return to Janitor Monk
    -   Reward(s):
        -   50,000 exp
-   The Secret of the Sutra Depository 2
    -   China Quest
    -   Item(s) Needed:
        -   Silver Spearman’s Heart x 50
    -   NPC(s) Involved:
        -   Janitor Monk
        -   Zheung Guan
    -   Procedures:
        -   Talk to Janitor Monk
        -   Eliminate 80 Silver Spearman and gather 50 Silver Spearman’s Heart
        -   Deliver the Silver Spearman’s Hearts to Zheung Guan on the 3rd floor
    -   Reward(s):
        -   250,000 exp
-   The Secret of the Sutra Depository 3
    -   China Quest
    -   Item(s) Needed:
        -   Gold Martial Artist’s Heart x 50
    -   NPC(s) Involved:
        -   Zheung Guan
        -   Cheng Xin
    -   Procedures:
        -   Talk to Zheung Guan
        -   Eliminate 50 Mini Gold Martial Artist and gather 50 Gold Martial Artist’s Heart
        -   Deliver the Gold Martial Artist’s Hearts to Cheng Xin
    -   Reward(s):
        -   1,250,000 exp
-   The Secret of the Sutra Depository 4
    -   China Quest
    -   Item(s) Needed:
        -   Gold Martial Artist’s Heart x 30
        -   Silver Spearman’s Heart x 30
        -   Bronze Martial Artist’s Heart x 30
    -   NPC(s) Involved:
        -   Cheng Xin
        -   Janitor Monk
    -   Procedures:
        -   Talk to Cheng Xin
        -   Gather 30 Gold Martial Artist’s Heart, 30 Silver Spearman’s Heart, and 30 Bronze Martial Artist’s Heart
        -   Deliver the hearts to Janitor Monk
    -   Reward(s):
        -   2,500,000 exp
    -   Remark(s):
        -   “Take someone with you to check out the Picture of Damo at the top of the Sutra Depository, then you should be able to find out the truth.”

### Thailand

#### A Morning at the Temple

-   A Morning at the Temple (Available to all)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Fallen Leaf x 20
    -   NPC(s) Involved:
        -   Janitor Monk
    -   Procedures:
        -   Talk to Janitor Monk
        -   Gather 20 Fallen Leaves by cleaning the fallen leaf piles in the forest around the monastery
        -   Return to Janitor Monk
    -   Reward(s):
        -   4,000 exp
    -   Random Reward(s):
        -   5 Topaz Ore (on my attempt)
        -   More ores?

#### Reciting the Buddhist Scriptures

-   Reciting the Buddhist Scriptures 1, 2 (Available to all)
    -   Thailand Quest
    -   Pre-requisite: A Morning at the Temple
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Janitor Monk
        -   Abbot
        -   Old Abbot
    -   Procedures:
        -   Talk to Janitor Monk
        -   Talk to the Abbot
        -   Talk to the Old Abbot
        -   Return to the Janitor Monk
    -   Random Reward(s):
        -   Scroll for Gloves for DEX 10% (on my attempt)
        -   More scrolls?

#### Entering the Floating Market Protection Squad

-   Entering the Floating Market Protection Squad 1, 2, 3, 4, 5, 6 (Over Level 10)
    -   Thailand Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Chai
        -   Kith
        -   Pond
        -   Lung Tup
        -   Pooyai Lee
        -   Jit
    -   Procedures:
        -   Talk to Chai
        -   Talk to Kith in the boat to the right of Chai
        -   Talk to Pond, left of Spinel
        -   Talk to Lung Tup slightly right of Pond
        -   Talk to Pooyai Lee at Thailand: Isolated House
        -   Talk to Jit at Thailand: The outskirt of Village
        -   Return to Chai at Thailand: Floating Market
    -   Reward(s):
        -   3,500 exp
        -   3,000 Mesos
        -   3 Fame

#### The Welcoming Ritual

-   The Welcoming Ritual 1, 2, 3, 4, 5 (Over Level 10)
    -   Thailand Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Pooyai Lee
    -   Procedures:
        -   Talk to Pooyai Lee
        -   Gather Rice, a Banana Flower, a Banana Leaf, and a White Silk Thread from the residents of Floating Market
        -   Talk to Nid to receive a Strange Flower (apparently a Banana Flower)
        -   Talk to Ponlaa to receive a Banana Leaf
        -   Talk to Lung Tup to receive a Sack of Rice
        -   Talk to Jit to receive Silk Thread
        -   Return to Pooyai Lee
    -   Reward(s):
        -   3,000 Mesos
        -   1 Fame
        -   Thai Cookie x 50

#### Sick Snake Needs Food

-   Sick Snake Needs Food I (Over Level 15)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Chicken x 50
        -   White Egg x 50
    -   NPC(s) Involved:
        -   Apaporn
    -   Procedures:
        -   Talk to Apaporn
        -   Gather 50 Chickens and 50 White Eggs from White Rooster
        -   Return to Apaporn
    -   Reward(s):
        -   1,650 exp
        -   3,000 Mesos
        -   1 Fame
    -   Random Reward(s):
        -   Antidote x 50
        -   Eyedrop x 50
        -   Tonic x 50
        -   Holy Water x 50
-   Sick Snake Needs Food II (Over Level 20)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Chicken Feet x 50
    -   NPC(s) Involved:
        -   Apaporn
    -   Procedures:
        -   Talk to Apaporn
        -   Gather 50 Chicken Feet from Rooster
        -   Return to Apaporn
    -   Reward(s):
        -   2,000 exp
        -   3,500 Mesos
        -   All Cure Potion x 20

#### Floating Market Protection Squad I

-   Eliminating Jr. Necki and Toad
    -   Thailand Quest
    -   Pre-requisite: Entering the Floating Market Protection Squad
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Chai
    -   Procedures:
        -   Talk to Chai
        -   Kill 50 Jr. Neckis and 50 Toads
        -   Return to Chai
    -   Reward(s):
        -   5,200 exp
        -   5,000 Mesos

#### Floating Market Protection Squad II

-   The Lost Key
    -   Thailand Quest
    -   Item(s) Needed:
        -   Kid’s Key x 1
    -   NPC(s) Involved:
        -   Chai
    -   Procedures:
        -   Talk to Chai
        -   Defeat Red Lizards to obtain Kith’s keys
        -   Return to Chai
    -   Reward(s):
        -   8,000 exp
        -   Power Elixir x 20

#### The Plan to Win His Heart

-   The Plan to Win His Heart (Over Level 25)
    -   Thailand Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Apaporn
        -   Pooyai Lee
    -   Procedures:
        -   Talk to Apaporn
        -   Talk to Chief Pooyai Lee and ask him to make a potion called The Seducer
    -   Reward(s):
        -   None
-   The Chief Refuses (Over Level 25)
    -   Thailand Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Pooyai Lee
        -   Apaporn
    -   Procedures:
        -   Talk to Apaporn
        -   Talk to Pooyai Lee
    -   Reward(s):
        -   None
-   Chief’s Fruit Pot
    -   Thailand Quest
    -   Item(s) Needed:
        -   Preserved Fruit Pot
    -   NPC(s) Involved:
        -   Apaporn
    -   Procedures:
        -   Talk to Apaporn
        -   Go to Thailand: Frog Pond and break the dark rock at the top left of the map to receive Preserved Fruit Pot
        -   Show the Preserved Fruit Pot to Pooyai Lee
    -   Reward(s):
        -   None
-   The Seducer and Its Strange Ingredients
    -   Thailand Quest
    -   Item(s) Needed:
        -   Toad Poison x 50
        -   Yellow Lizard Tail x 50
    -   NPC(s) Involved:
        -   Pooyai Lee
    -   Procedures:
        -   Talk to Pooyai Lee
        -   Gather 50 Toad Poison from Toads and 50 Yellow Lizard Tails from Yellow Lizards
        -   Return to Pooyai Lee
    -   Reward(s):
        -   The Seducer
-   The Seducer
    -   Thailand Quest
    -   Item(s) Needed:
        -   The Seducer
    -   NPC(s) Involved:
        -   Pooyai Lee
        -   Apaporn
    -   Procedures:
        -   Talk to Pooyai Lee
        -   Bring The Seducer to Apaporn
    -   Reward(s):
        -   4,250 exp
        -   Mana Elixir x 50

#### Seeking Permission from the village Chief

-   Missing an Old Friend (Over Level 30)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Letter from the Old Abbot
    -   NPC(s) Involved:
        -   Old Abbot
        -   Pooyai Lee
    -   Procedures:
        -   Talk to the Old Abbot
        -   Find “Iriad” (Pooyai Lee) at Thailand: Isolated House and deliver the Letter from the Old Abbot
    -   Reward(s):
        -   Nothing
-   A letter from faraway
    -   Thailand Quest
    -   Item(s) Needed:
        -   A letter from Pooyai Lee
    -   NPC(s) Involved:
        -   Pooyai Lee
        -   Old Abbot
    -   Procedures:
        -   Talk to Pooyai Lee to receive A letter from Pooyai Lee
        -   Deliver the letter to Old Abbot
    -   Reward(s):
        -   6,000 exp
        -   2 Fame

#### Chai’s First Crush

-   Chai’s First Crush 1, 2, 3 (Over Level 52)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Croco Skin x 50
    -   NPC(s) Involved:
        -   Chai
        -   Chut
    -   Procedures:
        -   Talk to Chai
        -   Talk to Chut
        -   Gather 50 Croco Skins from Croco
        -   Return to Chut
        -   Bring Chai’s Love Letter (for Nid) to Chai
    -   Reward(s):
        -   18,500 exp
        -   Mana Elixir x 30

#### Gathering information

-   Gathering information 1-5 (Over Level 60)
    -   Thailand Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Old Abbot
        -   Pooyai Lee
        -   Lung Tup
        -   Jit
        -   Captain Nareth
        -   Apaporn
    -   Procedures:
        -   Talk to the Old Abbot
        -   Gather information from the locals about the Ghost Cave
        -   Speak to “Iriad” (Pooyai Lee) at Thailand: Isolated House
        -   Talk to “Long Tup” (Lung Tup) at Thailand: Floating Market left of Spinel
        -   Talk to “Sherbet” (Jit) at Thailand: The outskirt of Village
        -   Talk to Captain Nareth at Thailand: Floating Market directly left of Spinel
        -   Talk to Apaporn at Thailand: Toad Pond
    -   Reward(s):
        -   10,000 exp
-   Gathering information 6
    -   Thailand Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Apaporn
        -   Old Abbot
    -   Procedures:
        -   Talk to Apaporn at Thailand: Toad Pond
        -   Report back to the Old Abbot at Thailand: Golden Temple
    -   Reward(s):
        -   25,000 exp
    -   Random Reward(s):
        -   Scroll for Topwear for DEF 60% (on my attempt)
        -   More scrolls?

#### Jit Hates Snakes

-   Jit Hates Snakes (Over Level 60)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Snake Scale x 50
    -   NPC(s) Involved:
        -   Jit
    -   Procedures:
        -   Talk to Jit
        -   Eliminate 50 Pythons and gather 50 Snake Scales
        -   Return to Jit
    -   Reward(s):
        -   8,900 exp
    -   Random Reward(s):
        -   Massage Oil x 10
        -   Muey Thai String

#### The Invasion of the Wild Animals

-   The Invasion of the Wild Animals (Over Level 65)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Tourist’s Camera
        -   Tourist’s Phone
    -   NPC(s) Involved:
        -   Abbot
    -   Procedures:
        -   Talk to the Abbot
        -   Retrieve the Tourist’s Camera and Tourist’s Phone from Wild Monkeys
        -   Return to the Abbot
    -   Reward(s):
        -   10,000 exp

#### Mischievous little monkeys

-   Mischievous little monkeys (Over Level 65)
    -   Thailand Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   She Long
    -   Procedures:
        -   Talk to She Long
        -   Eliminate 50 Wild Monkey and 50 Mama Monkey
        -   Return to She Long
    -   Reward(s):
        -   12,000 exp
        -   Guava x 20

#### The Truth of the issue

-   The Truth of the issue 1 / The Truth about the issue 2
    -   Thailand Quest
    -   Pre-requisite: Mischievous little monkeys
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   She Long
        -   Nid
    -   Procedures:
        -   Talk to She Long
        -   Check on Nid at Thailand: Floating Market
        -   Return to She Long with the good news
    -   Reward(s):
        -   18,500 exp
    -   Random Reward(s):
        -   Nothing (on my attempt)
        -   More random rewards?

#### Sealing Ravana

-   Sacred Soul 1, 2 (Over Level 65)
    -   Thailand Quest
    -   Pre-requisite: Gathering information
    -   Item(s) Needed:
        -   Power Crystal x 5
        -   Red Goblin’s Axe x 50
    -   NPC(s) Involved:
        -   Guard
    -   Procedures:
        -   Talk to the Guard at Thailand: Golden Temple
        -   Talk to the Old Abbot for an explanation
        -   Gather 5 Power Crystals from refining and 50 Red Goblin’s Axes from Red Goblins
        -   Return to the Old Abbot
    -   Reward(s):
        -   20,000 exp
        -   Bloodied Axe
-   Sacred Soul 3, 4
    -   Thailand Quest
    -   Item(s) Needed:
        -   LUK Crystal x 5
        -   Blue Goblin’s Crown x 50
    -   NPC(s) Involved:
        -   Old Abbot
        -   Batama
    -   Procedures:
        -   Talk to the Old Abbot
        -   Talk to Batama
        -   Gather 5 LUK Crystals from refining and 50 Blue Goblin’s Crown from Blue Goblins
        -   Return to Batama
    -   Reward(s):
        -   20,000 exp
        -   Phoenix Crown
-   Sacred Soul 5, 6
    -   Thailand Quest
    -   Item(s) Needed:
        -   Dark Crystal
        -   Wisdom Crystal
        -   DEX Crystal
        -   Stone Goblins’ Red Undergarments x 50
    -   NPC(s) Involved:
        -   Batama
        -   Abbot
    -   Procedures:
        -   Talk to Batama
        -   Talk to the Abbot
        -   Gather Dark Crystal, DEX Crystal, Wisdom Crystal, and 50 Stone Goblins’ Red Undergarments
        -   Return to the Abbot
    -   Reward(s):
        -   20,000 exp
        -   Cloth from the sky
-   Sacred Soul 7
    -   Thailand Quest
    -   Item(s) Needed:
        -   Bloodied Axe
        -   Phoenix Crown
        -   Cloth from the sky
    -   NPC(s) Involved:
        -   Abbot
        -   Old Abbot
    -   Procedures:
        -   Talk to the Abbot
        -   Bring the Bloodied Axe, Phoenix Crown, and Cloth from the sky to the Guard
    -   Reward(s):
        -   42,000 exp
        -   Magical Stone Piece
        -   Sunburst x 3
        -   Elixir x 50
        -   Guava x 10
        -   Scroll for Bottomwear for DEF 60% (on my attempt)
        -   More random scrolls?

#### The Final Stage of the Sealing Ritual

-   The Final Stage of the Sealing Ritual (Over Level 65)
    -   Thailand Quest
    -   Pre-requisite: Sealing Ravana
    -   Item(s) Needed:
        -   Magical Stone Piece (required for party leader)
        -   Sunburst (for summoning Ravana)
        -   Ravana Doll
    -   NPC(s) Involved:
        -   Guard
        -   Old Abbot
    -   Procedures:
        -   Talk to the Guard
        -   Defeat Ravana to obtain the Ravana Doll
        -   Bring the Ravana Doll to the Old Abbot
        -   You will lose the Ravana Doll and Magical Stone Piece on quest completion
    -   Reward(s):
        -   45,000 exp
    -   Random Reward(s):
        -   Ravana Helmet
        -   Ravana Helmet (I)
        -   Ravana Helmet (II)
        -   Ravana Helmet (III)

#### The Unfinished Battle

-   The Unfinished Battle (Over Level 65)
    -   Thailand Quest
    -   Item(s) Needed:
        -   Monkey Doll x 50
        -   Banana Peel x 50
        -   Red Goblin’s Axe x 20
        -   Blue Goblin’s Crown x 20
        -   Stone Goblins’ Red Undergarments
    -   NPC(s) Involved:
        -   Guard
    -   Procedures:
        -   Talk to the Guard
        -   Gather 50 Monkey Dolls, 50 Banana Peels, 20 Red Goblin’s Axes, 20 Blue Goblin’s Crowns, and 20 Stone Goblin’s Red Undergarments
        -   Return to the Guard
    -   Reward(s):
        -   10,500 exp
        -   Magical Stone Piece
        -   Sunburst x 2
    -   Remark(s):
        -   This quest is repeatable.

### Zipangu

#### Kino Konoko’s Concern

-   Kino Konoko’s Concern (Level 20 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Crow Feather x 100
    -   NPC(s) Involved:
        -   Kino Konoko
    -   Procedures:
        -   Talk to Kino Konoko in Zipangu: Mushroom Shrine.
        -   Hunt for 100 Crow Feather.
        -   Return to her after you have completed the task.
    -   Reward(s):
        -   1,700 experience

#### Hanako’s Foxtail Wardrobe (Level 25 and above)

-   Hanako’s Foxtail Muffler 1
    -   World Tour Quest
    -   Item(s) Needed:
        -   Cloud Foxtail x 100
    -   NPC(s) Involved:
        -   Hanako
    -   Procedures:
        -   Talk to Hanako in Zipangu: Showa Town.
        -   Hunt for 100 Cloud Foxtail.
        -   Return to her after you have collected the item.
    -   Reward(s):
        -   4,000 experience
-   Hanako’s Foxtail Muffler 2
    -   World Tour Quest
    -   Item(s) Needed:
        -   Cloud Foxtail x 200
    -   NPC(s) Involved:
        -   Hanako
    -   Procedures:
        -   Talk to Hanako in Zipangu: Showa Town.
        -   Hunt for 200 Cloud Foxtail.
        -   Return to her after you have collected the item.
    -   Reward(s):
        -   6,000 experience
-   Hanako’s Foxtail Coat
    -   World Tour Quest
    -   Item(s) Needed:
        -   Cloud Foxtail x 400
    -   NPC(s) Involved:
        -   Hanako
    -   Procedures:
        -   Talk to Hanako in Zipangu: Showa Town.
        -   Hunt for 400 Cloud Foxtail.
        -   Return to her after you have collected the item.
    -   Reward(s):
        -   10,000 experience

#### Kino Konoko vs Fire Raccoon

-   Kino Konoko vs Fire Raccoon (Level 25 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Kino Konoko
    -   Procedures:
        -   Talk to Kino Konoko in Zipangu: Mushroom Shrine.
        -   Kill 80 Flaming Raccoon.
        -   Return to her after you have completed the task.
    -   Reward(s):
        -   3,500 experience
        -   Fish Cake (Dish) x 10

#### Read the Newspaper!

-   Read the Newspaper! (Level 25 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Newspaper Hat x 1
    -   NPC(s) Involved:
        -   Tsuri
    -   Procedures:
        -   Talk to Tsuri in Zipangu: Showa Town.
        -   Obtain 1 Newspaper Hat.
        -   Go back to Tsuri and give him the collected item.
    -   Reward(s):
        -   3,000 experience
        -   Dark Scroll for Overall Armor for DEX 30% x 1

#### The Lantern at Mushroom Shrine

-   The Lantern at Mushroom Shrine (Level 35 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Worn Paper Lantern x 50
    -   NPC(s) Involved:
        -   Hikari
    -   Procedures:
        -   Talk to Hikari in Zipangu: Showa Town.
        -   Hunt for 50 Worn Paper Lantern from Paper Lantern Ghost.
        -   Kill 100 Paper Lantern Ghost.
        -   Return to Hikari once you have completed the task.
    -   Reward(s):
        -   10,500 experience

#### A Little Mischief

-   A Little Mischief (Level 40 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Chunin Poison Dart x 100
    -   NPC(s) Involved:
        -   Kamedeya
    -   Procedures:
        -   Talk to Kamedeya at Zipangu: Outside Ninja Castle.
        -   Hunt for 100 Chunin Poison Dart from Chunin.
        -   Return and talk to Kamedeya.
    -   Reward(s):
    -   Random Reward(s):
        -   Dark Scroll for Face Accessory for HP 30% x 1
        -   Dark Scroll for Face Accessory for HP 70% x 1

#### Eliminating Dark Cloud Fox

-   Eliminating Dark Cloud Fox (Level 40 and above)
    -   World Tour Quest
    -   Pre-requisite: Hanako’s Foxtail Wardrobe
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Hanako
    -   Procedures:
        -   Talk to Hanako in Zipangu: Showa Town.
        -   Kill 80 Dark Cloud Fox.
        -   Return to her after you have completed the task.
    -   Reward(s):
        -   10,500 experience
        -   Takoyaki (Octopus Ball) x 5
    -   Remark(s):
        -   Dark Cloud Fox AKA Giant Cloud Fox

#### Mariwaka of Performer Training

-   Mariwaka of Performer Training (Level 40 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Mariwaka
    -   Procedures:
        -   Look for Mariwaka in Zipangu: Showa Town.
        -   Kill 10 Leader A and 10 Leader B.
        -   Go back to Mariwaka once you have completed the task.
    -   Reward(s):
        -   Takoyaki (Jumbo) x 10
        -   Yakisoba (x2) x 10
    -   Remark(s):
        -   This quest is only for levels below 40.

#### The Secret of Ninja Castle (Level 40 and above)

-   Helping Others (Level 40 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Aoi
        -   Nagare
    -   Procedures:
        -   Talk to Aoi at Zipangu: Outside Ninja Castle.
        -   Then talk to Nagare at Zipangu: Inside the Castle Gate.
    -   Reward(s):
    -   Random Reward(s):
        -   Scroll for Face Accessory for HP 10% x 1
        -   Scroll for Face Accessory for HP 60% x 1
        -   Scroll for Face Accessory for HP 100% x 1

#### Find Mariwaka’s Purse! (Level 45 and above)

-   Grako’s Request (Level 45 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Cider x 1
    -   NPC(s) Involved:
        -   Grako
        -   Doran
    -   Procedures:
        -   Talk to Grako in Zipangu: Showa Town.
        -   Purchase 1 Cider from Doran.
        -   Return to Grako after purchasing the item.
    -   None
-   Grako’s Request 2 (Level 45 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Grako
        -   Mariwaka
    -   Procedures:
        -   Talk to Grako once again.
        -   Search for Mariwaka in Zipangu: Showa Town and talk to her.
    -   None
-   Mariwaka’s Request (Level 45 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Popo
    -   Procedures:
        -   Search for Popo in Zipangu: Showa Town and speak to him.
    -   None
-   Mariwaka’s Purse (Level 45 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Mariwaka’s Bag x 1
    -   NPC(s) Involved:
        -   Mariwaka
    -   Procedures:
        -   Hunt for 1 Mariwaka’s Bag from Leader A or Leader B.
        -   Give the collected item back to Mariwaka.
    -   Reward(s):
        -   All Cure Potion x 20

#### The Secret of Ninja Castle (Level 40 and above)

-   Cooperation (Level 50 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Kunoichi Dagger x 50
    -   NPC(s) Involved:
        -   Nagare
        -   Falcon Trainer
    -   Procedures:
        -   Talk to Nagare at Zipangu: Inside the Castle Gate.
        -   Hunt for 50 Kunoichi Dagger from Kunoichi.
        -   Then talk to Aoi at Zipangu: Inside the Castle Gate.
    -   Reward(s):
    -   Random Reward(s):
        -   Scroll for Face Accessory for Avoidability 10% x 1
        -   Scroll for Face Accessory for Avoidability 60% x 1
        -   Scroll for Face Accessory for Avoidability 100% x 1

#### Eliminating Ghosts

-   Eliminating Ghosts (Level 55 and above)
    -   World Tour Quest
    -   Pre-requisite: The Lantern at Mushroom Shrine
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Hikari
    -   Procedures:
        -   Talk to Hikari in Zipangu: Showa Town.
        -   Kill 100 Nightghost.
        -   Return to Hikari once you have completed the task.
    -   Reward(s):
        -   20,600 experience

#### Water Goblin’s Cucumber

-   Water Goblin’s Cucumber (Level 55 and above)
    -   World Tour Quest
    -   Pre-requisite: Eliminating Ghosts
    -   Item(s) Needed:
        -   Cucumber x 40
    -   NPC(s) Involved:
        -   Hikari
    -   Procedures:
        -   Talk to Hikari in Zipangu: Showa Town.
        -   Hunt for 40 Cucumber.
        -   Kill 80 Water Goblin.
        -   Return to Hikari once you have completed the tasks.
    -   Reward(s):
        -   21,500 experience
    -   Remark(s):
        -   This quest is glitched. Although it says you need 40 cucumbers, you must actually collect 50.

#### The Secret of Ninja Castle (Level 40 and above)

-   Collecting Info (Level 60 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Ninto Scroll x 50
        -   Kacchuu Musha Fragment x 1
    -   NPC(s) Involved:
        -   Falcon Trainer
        -   Anz
    -   Procedures:
        -   Talk to Aoi at Zipangu: Inside the Castle Gate.
        -   Hunt for 50 Ninto Scroll from Ninto and 1 Kacchuu Musha Fragment from Kacchuu Musha.
        -   Then talk to Anz at Zipangu: Inside the Castle Gate.
    -   Reward(s):
    -   Random Reward(s):
        -   Scroll for Eye Accessory for Accuracy 10% x 1
        -   Scroll for Eye Accessory for Accuracy 60% x 1
        -   Scroll for Eye Accessory for Accuracy 100% x 1
-   Defeat the Great Offender (Level 70 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Emperor Toad Wallet x 1
    -   NPC(s) Involved:
        -   Anz
    -   Procedures:
        -   Talk to Anz at Zipangu: Inside the Castle Gate.
        -   Hunt for 1 Emperor Toad Wallet by defeating Castellan Toad.
        -   Then return and talk to Anz.
    -   Reward(s):
        -   100,000 mesos
    -   Random Reward(s):
        -   Dark Scroll for Face Accessory for Avoidability 30% x 1
        -   Dark Scroll for Face Accessory for Avoidability 70% x 1
        -   Dark Scroll for Eye Accessory for Accuracy 30% x 1
        -   Dark Scroll for Eye Accessory for Accuracy 70% x 1
        -   Dark Scroll for Eye Accessory for INT 30% x 1
        -   Dark Scroll for Eye Accessory for INT 70% x 1
        -   Elixir x 10
-   A Peaceful World (Level 70 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Castellan’s Stolen Armrest x 1
    -   NPC(s) Involved:
        -   Anz
        -   Aoi
    -   Procedures:
        -   Talk to Anz at Zipangu: Inside the Castle Gate.
        -   Get the Castellan’s Stolen Armrest from Anz and then go and see Aoi at Zipangu: Outside Ninja Castle.
    -   Reward(s):
    -   Random Reward(s):
        -   Scroll for Eye Accessory for INT 10% x 1
        -   Scroll for Eye Accessory for INT 60% x 1
        -   Scroll for Eye Accessory for INT 100% x 1

#### Eliminating Blue Mushmom

-   Eliminating Blue Mushmom (Level 85 and above)
    -   Victoria Island Quest
    -   Pre-requisite: Water Goblin’s Cucumber
    -   Item(s) Needed:
        -   None
    -   NPC(s) Involved:
        -   Hikari
    -   Procedures:
        -   Talk to Hikari.
        -   Eliminate 1 Blue Mushmom.
        -   Return to Hikari once you have completed the task.
    -   Reward(s):
        -   19,800 experience
    -   Random Reward(s):
        -   Scroll for One-Handed Sword for ATT 60% x 1
        -   Scroll for One-Handed Axe for ATT 60% x 1
        -   Scroll for One-Handed BW for ATT 60% x 1
        -   Scroll for Dagger for ATT 60% x 1
        -   Scroll for Wand for Magic Attack 60% x 1
        -   Scroll for Staff for Magic Attack 60% x 1
        -   Scroll for Two-Handed Sword for ATT 60% x 1
        -   Scroll for Two-Handed Axe for ATT 60% x 1
        -   Scroll for Two-Handed BW for ATT 60% x 1
        -   Scroll for Spear for ATT 60% x 1
        -   Scroll for Pole Arm for ATT 60% x 1
        -   Scroll for Bow for ATT 60% x 1
        -   Scroll for Crossbow for ATT 60% x 1
        -   Scroll for Claw for ATT 60% x 1
        -   Scroll for Earring for INT 60% x 1
        -   Scroll for Cape for STR 60% x 1
        -   Scroll for Cape for INT 60% x 1
        -   Scroll for Cape for DEX 60% x 1
        -   Scroll for Cape for LUK 60% x 1
        -   Scroll for Gloves for ATT 60% x 1
        -   Scroll for Knuckler for ATT 60% x 1
        -   Scroll for Gun for Attack 60% x 1
        -   Power Elixir x 20
        -   Elixir x 40
        -   Sword Earrings x 1
    -   Remark(s):
        -   This quest is repeatable.

#### Veggies are Awesome

-   Veggies are Awesome (Level 85 and above)
    -   World Tour Quest
    -   Item(s) Needed:
        -   Bain’s Spiky Collar x 200
    -   NPC(s) Involved:
        -   Umi
    -   Procedures:
        -   Talk to Umi in Zipangu: Showa Town.
        -   Collect 200 Bain’s Spiky Collar from Bain.
        -   Return to Umi after you have collected the item.
    -   Reward(s):
        -   Watermelon x 50
