# World Tour Lock

-   [Introduction](introduction.md)
-   [Rules](rules.md)
-   [Monsters](monsters.md)
-   [Quests](quests.md)
-   [4th Job Skills](4th_job_skills.md)
-   [Equipment](equipment.md)
-   [Scrolls](scrolls.md)
-   [Consumables](consumables.md)
-   [Collectibles](collectibles.md)
-   [FAQ](faq.md)
-   [Glossary](glossary.md)

## Introduction

![Spinel](./images/spinel_world_tour.png)

“If you’re tired of the monotonous daily life, how about going out for a change? There’s nothing quite like soaking up a new culture, and learning something new by the minute! It’s time for you to get out and travel. We, at the Maple Travel Agency recommend that you go on a World Tour!”

### 真・国際観光無双・ザワールドツアー

しん・こくさいかんこうむそう・ザワールドツアー

“True・International Tourism Unparalleled・THE WORLD TOUR”

#### The World Tour Challenge

-   A special area-locked gameplay challenge for MapleStory; explore a variety of travel destinations in the Orient and never leave!
-   For a small fee, the Maple Travel Agency will accomodate new Tourists on the trip of a lifetime to the Far East. The environment may be exceedingly hostile for a vacation destination, but in time you’ll hone your abilities and gain the trust of the locals. Establish your place in a different society, culture, and even time period!
-   This challenge is based on the MapleLegends (GMS v62-based) private server implementation of the World Tour areas.
-   Encompasses all of Spinel’s World Tour destinations:
    -   Shanghai of China
    -   Floating Market of Thailand
    -   Mushroom Shrine of Japan
    -   Shaolin Temple of China
    -   Golden Temple of Thailand

#### Notable Features of the World Tour Challenge

-   Hardcore grinding for equipment upgrades is heavily encouraged
-   You can use the Gachapon! Yes, just like in Japan
-   Utilize the Showa Exchange to its full potential
-   Several area bosses, mostly located in Japan
-   Some expedition bosses including Ravana and those in Neo Tokyo
-   Collect all of the items based on those in the Romance of the Three Kingdoms
-   Beautiful views of the Shanghai skyline
-   Terrifying encounters with Yokai
-   Shining gold Buddhist temples hidden deep in the thick jungle
-   A laid-back experience away from the pressures of Maple World
-   Contains content all the way up to the traditional “end game” of MapleStory, while keeping in line with the theme of the challenge

### Is it fun? Is it balanced?

-   World Tour area-lock is a niche within a niche, and I don’t expect many players to want to take part. The challenge is all about grinding hard for your own equipment and scrolls, exploring sometimes deeply underutilized maps and monsters, and appreciating some of the cultural references present in these cute, miniaturized versions of real life areas and their local folklore. Slide on your wooden geta and clack down the streets towards a new adventure.
-   Keep in mind, this challenge is unlikely to be very balanced. Depending on your job of choice, your character could face many levels in between equipment upgrades. You could have to grind low EXP monsters to obtain the scrolls you need, if they’re even available at all. Many Mastery Books and even some Skill Books aren’t obtainable, so all characters will in some way have their progression cut short compared to their vanilla counterparts in Maple World. Is the end game of Neo Tokyo even possible for these characters? Honestly, I’m too casual of a player to know. Look carefully at the available equipment, scrolls, and skills available before choosing your job!

## Rules

-   The World Tour Challenge, its restrictions, and its exceptions may be altered over time to suit the needs of its participants. Generally, the philosophy here is “less is more” – the least amount of content possible to create a cohesive area-lock challenge should be available initially, with additional exceptions carved out over time only as needed.
-   Characters participating in the World Tour Challenge are referred to as Tourists. Players NOT participating in the challenge are referred to as non-Tourists.
-   The entirety of Spinel’s World Tour (the areas in which gameplay is locked to for this version of the World Tour Challenge) is sometimes generically referred to as the World Tour. While this does not include every actual World Tour area available within MapleStory, it serves as a shorthand for “everywhere Tourists are normally allowed to access.”

### Restrictions

-   Area-Lock
    -   Tourists must enter Spinel’s World Tour at level 10 or earlier
    -   Upon entry, Tourists may only travel between Spinel’s World Tour regions, and may never leave this combined zone unless an exception allows it
    -   If an exception allows leaving the World Tour, return immediately after finishing the allowed content
-   Items and Looting
    -   Immediately before entering World Tour for the first time, Tourists must drop, sell or give away all items and Mesos except for:
        -   Starter weapon(s) (must be from character creation or provided by 1st job instructor)
        -   Starter ammo provided by 1st job instructor
        -   Exactly enough Mesos for Spinel’s travel fee (3000 Mesos, or 300 for Beginners)
    -   When outside the World Tour, picking up items or Mesos is forbidden
        -   Do not allow pets to loot when outside the World Tour
    -   Trading with NPCs outside the World Tour is forbidden
        -   Myo Myo the Traveling Salesman is considered an NPC outside the World Tour, and cannot be used
    -   Picking up items and/or Mesos that do not belong to a Tourist is forbidden
        -   If the item cannot be picked up immediately (the Tourist attempts to loot an item, but the item is under a wait period), the item does not belong to the Tourist
    -   Use of the Gachapon is only permitted when Tourists are both:
        -   Within the World Tour
        -   Over level 30
    -   Mastery Books may only be used if obtained within the World Tour
-   Partying and Multiplayer Interactions
    -   No receiving buffs from buff mules
    -   Map wide buffs are forbidden in most cases
        -   Echo of Hero is forbidden, unless provided by a level 200 Tourist
        -   GM buffs are forbidden
        -   Weather effect buffs are forbidden
    -   Right-click to cancel any forbidden buffs
    -   No receiving leech
    -   When in a party with non-Tourists, Tourists may only loot items that belong to a Tourist
        -   Tourists may only loot items from monsters or bosses that were whited by a Tourist, meaning that a Tourist dealt the most damage to the monster
        -   Cheesing this restriction through some unforeseen means, such as intentionally weakening a boss with a non-Tourist before slaying it, is forbidden
-   Questing
    -   Tourists may only complete quests located within Spinel’s World Tour that can be completed without leaving
-   Trading and Transferring
    -   Receiving items or Mesos from non-Tourists is forbidden
    -   Transfer of items through any means from non-Tourists to Tourists is forbidden
        -   Includes any cash shop item that is not purely cosmetic

### Exceptions

-   Aspiring Tourists (characters level 10 or lower that have not yet entered the World Tour) may pick up items (EXCLUDING monster cards) and Mesos from any monster in MapleStory, pick up items (EXCLUDING Gachapon tickets, monster cards, and NX) and Mesos owned by non-Tourists, and sell items to any NPC in MapleStory; upon entering the World Tour for the first time, this exception is forever void
-   Tourists may leave the World Tour to access the following content ONLY as needed:
    -   Job advancement
    -   4th job skill quests and Skill Books
        -   When a skill quest requires defeating a boss located outside the World Tour, the Tourist must solo that boss
        -   The following content is permitted for the purpose of unlocking 4th job skills:
            -   El Nath PQ
            -   Horntail PQ (including Horntail prequests)
            -   Zakum prequests and boss fight entry (Tourists MUST fight Zakum completely solo)
    -   Pet resurrection miniquests from Mar the Fairy or Wisp
    -   Follow the Lead miniquest/jump quest
    -   the Free Market (trading restrictions still apply)
    -   refining NPCs to refine ore into plates, crystals, and screws
    -   “permanent” character cosmetic changes if unavailable in the World Tour (hairstyles, faces, and skin colors)
    -   event-exclusive cosmetic items, hairstyles, faces, and skin colors
    -   expansion of friends list
    -   creating “A set of Match Cards” using materials obtained within the World Tour
-   Tourists may leave the World Tour to obtain the following items from the monsters that drop them ONLY as needed:
    -   Items required for starting 4th job Skill Book quests, such as \[Storybook\] items: Ancient Book, Black Book, Burning Book of Fire, Crimson Balrog’s Proposal, Formula for Black Cloud, Frozen Book of Ice, Indecipherable Book, or Unknown Letter
-   When partied with non-Tourists, receiving party-split Mesos is permitted
-   Event locations, NPCs, quests, and services may be accessed if:
    -   the event NPC is present within a World Tour map
    -   an NPC within a World Tour map warps the player to an event-exclusive map
-   Other event features may be permitted on a case-by-case basis, given the variability of event features
    - Leaving the World Tour is permitted to complete the following quest(s):
        -   “What does the moon look like?” from Baby Moon Bunny (Lunar New Year 2023)


### Personalized Restrictions

-   Tourists may set their own personal gameplay restrictions in addition to those previously listed

## Legal

[![CC BY-NC-SA 4.0](./images/cc_by_nc_sa.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

